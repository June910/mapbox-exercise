import React from 'react';
import logo from './logo.svg';
import './App.css';
// import Map from './components/map/Map';
import ReducerPage from './page/ReducerPage';
import UseReducerPage from './page/useReducerPage';
import Test from './components/Test';
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import HomePage from "@/pages/HomePage";
import LoginPage from "@/pages/LoginPage";
import UserPage from "@/pages/UserPage";
import PrivateRouter from '@/pages/PrivateRouter';
import Map from './mapboxDemo/Map'
import Space from './mapboxDemo/threeJsBuilding/Space';
import SmartCity from './mapboxDemo/smartCity/SmartCity';
import BasicEffectComposer from './mapboxDemo/threejsComposer/BasicEffectComposer';
import PostProcessing from './mapboxDemo/threejsComposer/PostProcessing';
import Gitch from './mapboxDemo/threejsComposer/Gitch';
import Mask from './mapboxDemo/threejsComposer/Mask';
import SelectObject from './mapboxDemo/threejsComposer/SelectObject';




function App() {
  return (
    <div className="App">
      {/* <BrowserRouter>
        <Link to="/">首页</Link>
        <Link to="/login">登录页</Link>
        <Link to="/user">用户中心</Link>
        <Link to="/aaa">404</Link>
        <Switch>
          <Route exact path='/' component={HomePage} />
          <Route path='/login' component={LoginPage} />
          <PrivateRouter path='/user' component={UserPage} />
          <Route component={() => <div>404</div>} />
        </Switch>
      </BrowserRouter> */}

      {/* <Test/> */}
      {/* <Map/> */}
      {/* <Space></Space> */}
      {/* <SmartCity></SmartCity> */}
      {/* <ReducerPage /> */}
      {/* <UseReducerPage /> */}
      {/* {<BasicEffectComposer></BasicEffectComposer>} */}
      {/* <PostProcessing></PostProcessing> */}
      {/* {<Gitch></Gitch>} */}
      {/* {<Mask></Mask>} */}
      <SelectObject></SelectObject>
    </div>
  );
}

export default App;
