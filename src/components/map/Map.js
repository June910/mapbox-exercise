import React, { Component } from 'react'
import './map.scss'
import mapboxgl from "mapbox-gl";
import { Provider } from "@/context/MapContext.js";
import ShowCoor from './showcoor/ShowCoor.js';
import Toggle from '../toggle/Toggle';
import worldData from '@/data/world';
import buildings from '@/data/buildings';
import { connect } from 'react-redux';
import { Button } from 'antd';
mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN;
const store = { userName: "June" };
class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            coorShown: [0, 0],
            map: null,
            height: 1
        }
    }
    shouldComponentUpdate(){
        return false
    }
    componentDidMount() {

        this.initializeMap();
    }
    // componentDidUpdate() {
    //     const { height } = this.state;
    //     console.log('update', height);
    //     const { map } = this.state;
    //     console.log('map',map);
    //     // let initHeight =map.getLayer('buildings')&& map.getPaintProperty('buildings','fill-extrusion-height') || {};

    //     map.getLayer('buildings') && map.setPaintProperty('buildings', 'fill-extrusion-height',
    //         ["+",
    //             ["*", ["get", "层数1"], 1],
    //         height])
    //     // map.getLayer('country-fills')&&this.setFill();
    // }
    static getDerivedStateFromProps(props,state){
        const { height } = state;
        const { map } = state;
        // let initHeight =map.getLayer('buildings')&& map.getPaintProperty('buildings','fill-extrusion-height') || {};
        map && map.getLayer('buildings') && map.setPaintProperty('buildings', 'fill-extrusion-height',
            ["+",
                ["*", ["get", "层数1"], 1],
            height])
        return null
    }
    setFill() {
        console.log('map', this.props);
        const { map } = this.state;
        const { property, stops } = this.props.active;
        map.setPaintProperty('country-fills', 'fill-color', {
            property,
            stops
        })

    }
    initializeMap() {
        const { height } = this.state;
        const map = new mapboxgl.Map({
            container: this.mapContainer,
            style: 'mapbox://styles/mapbox/dark-v10',
            center: [117.69, 39.02],
            zoom: 11,
            attributionControl: false
        })
        this.setState({
            map: map
        });
        map.on('load', () => {
            map.addSource('countries', {
                'type': 'geojson',
                'data': worldData
            });
            map.addSource('buildings', {
                'type': 'geojson',
                'data': buildings
            });
            // map.addLayer({
            //     'id': 'country-fills',
            //     'type': 'fill',
            //     'source': 'countries',
            //     'layout:': {},
            //     'paint': {}
            // });
            map.addLayer({
                'id': 'buildings',
                'type': 'fill-extrusion',
                'source': 'buildings',
                'layout:': {},
                'paint': {
                    'fill-extrusion-color': '#24acf2',
                    'fill-extrusion-height': ["*", ["get", "层数1"], 1],
                    'fill-extrusion-base': 0
                }
            });
            // this.setFill();
        });
    }

    // this.map.on('mousemove', e => {
    //     const { lng, lat } = e.lngLat;
    //     this.setState({
    //         coorShown: [lng, lat]
    //     })

    // })
    animationHeight = () => {
        const { height } = this.state;
        console.log('开始改变height');
        const runHigh = () => {
            this.setState({
                height: ++this.state.height
            });
            if (this.state.height < 50) {
                window.requestAnimationFrame(runHigh)
            }

        }
        runHigh();
    }
    render() {
        const { map } = this.state;
        console.log('render');
        return (
            <>
                <div
                    id="map"
                    ref={(el) => {
                        this.mapContainer = el
                    }}
                >
                    <Provider value={map} >
                        {map && <ShowCoor />}
                        <Toggle />
                    </Provider>
                    <div>
                        <Button onClick={this.animationHeight}>animation</Button>

                    </div>
                </div>
            </>
        )
    }
}
const mapStatusToProps = (state) => {
    return state.style
}
export default connect(
    mapStatusToProps
)(Map)