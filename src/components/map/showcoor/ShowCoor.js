import React, { Component } from 'react'
import './coorshow.scss'
import { Context, Consumer } from '@/context/MapContext.js'
import {useState,useEffect,useReducer,useContext,useMemo} from 'react';
const handleConsumer = Cmp => props => {
    return <Consumer>{ctx => <Cmp {...ctx} {...props} />}</Consumer>
}
// export default class ShowCoor extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             mousePositon: []
//         }
//     }
//     static contextType = Context;
//     componentDidMount() {
//         this.map = this.context;
//         this.map.on('mousemove', e => {
//             const { lng, lat } = e.lngLat;
//             this.setState({
//                 mousePositon: [lng, lat]
//             })

//         })
//     }
//     render() {
//         const { mousePositon } = this.state;
//         return (
//             <div id="coor-show">
//                 <span>经度：{Number(mousePositon[0]).toFixed(4)}</span>
//                 <span>纬度：{Number(mousePositon[1]).toFixed(4)}</span>
//             </div>
//         )
//     }
// }
export default function ShowCoor(){
    const [counter,setCounter] = useState(0);
    const [mousePosition, setMousePosition] = useState([]);
    const ctx = useContext(Context);
    const map = ctx;

useEffect(() => {
    map.on('mousemove', e => {
        let { lng, lat } = e.lngLat;
        setMousePosition([lng, lat]);
    });

    return () => {
        
    }
}, [])
    return (
        <div id="coor-show">
            <span>经度：{Number(mousePosition[0]).toFixed(4)}</span>
            <span>纬度：{Number(mousePosition[1]).toFixed(4)}</span>
            {/* <span>{counter}</span> */}
    {/* <span>{useClock().toLocaleTimeString()}</span> */}
            {/* <button onClick={change}>change</button> */}
        </div>
    )
}
function useClock(){
    const [date,setDate] = useState(new Date);
    useEffect(() => {
        const timer = setInterval(()=>{
            setDate(new Date);
        },1000);
        return () => {
            clearInterval(timer);
        }
    }, [])
    return date;

}

