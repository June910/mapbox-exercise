import { Radio } from 'antd';
import React, { Component } from 'react';
import {connect} from "react-redux";
import "./toggle.scss";
import {toggle} from "@/store/action/styleAction";
import {asyncAdd} from "@/store/action/counterAction";
 class Toggle extends Component {
    optionChange = (e)=>{
        const {styleOptions} = this.props.style;
        const {toggle} = this.props;
        const activeIndex = styleOptions.findIndex((option)=>{
           return option.name ===e.target.value
        });
        console.log(activeIndex);
        toggle(styleOptions[activeIndex]);
    }
    handler = ()=>{
        const {asyncAdd}  = this.props;
        asyncAdd()
    }
    render() {
        console.log('toggle',this.props);
        const {active,styleOptions} = this.props.style;
        // const {counter} = this.props.counter;
        return (
            <div id="toggle">
                {/* <span>{counter}</span> */}
                <button onClick={this.handler}>增加</button>
                <Radio.Group defaultValue={active.name} buttonStyle="solid" onChange={this.optionChange}>
                    {
                        styleOptions.map((item)=>{
                            return <Radio.Button value={item.name} key={item.name}>{item.name}</Radio.Button>
                        })
                    }
                </Radio.Group>
            </div>
        )
    }
}
const mapStateToProps = state=>{
    return state
}
const mapDispatchToProps = {
    toggle,
    asyncAdd
}
export default connect(mapStateToProps,mapDispatchToProps)(Toggle)
