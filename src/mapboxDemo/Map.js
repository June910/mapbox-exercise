import React, { Component } from 'react'
import './map.scss'
import mapboxgl from 'mapbox-gl';
import {Provider, Consumer} from '../context/MapContext';
import One from './demo/1.add-a-default-marker/One';
import Two from './demo/2.add-an-animated-icon/Two';
import Three from './demo/3.add-a-generated-icon/Three';
import Four from './demo/4.add-a-missing-icon/Four';
import Five from './demo/5.add-a-stretchable-image/Five';
import Twelve from './demo/12.extrude-polygons-for-3d/Twelve';
import Thirteen from './demo/13.diaplay-buildings-in-3d/Thirteen';
import Fifteen from './demo/15.adjust-a-layers-opacity/Fifteen';
import Fourteen from './demo/14.add-a-3d-model/Fourteen';
import Sixteen from './demo/16.add-animation-image/Sixteen';
import Seventeen from './demo/17.animate-a-line/Seventeen';
import Eighteen from './demo/18.animate-a-point/Eighteen';
import Nighteen from './demo/19.change-building-color-zoom-level/Nighteen';
import Twenty from './demo/20.create-and-style-cluster/Twenty';
import TwentyOne from './demo/21.Display-html-clusters-with-custom-properties/TwentyOne';
import TwentyTwo from './demo/22.change-the-case-of-labels/TwentyTwo';
import TwentyThree from './demo/23.add-a-pattern-to-a-polygon/TwentyThree';
import TwentyFour from './demo/24.style-circles-with-a-data-driven/TwentyFour';
import TwentyFive from './demo/25.create-a-heatmap-layer/TwentyFive';
import TwentySix from './demo/26.create-a-gradient-line-using-an-expression/TwentySix';
import TwentySeven from './demo/27.add-multiple-geometries/TwentySeven';
import TwentyEight from './demo/28.use-a-fallback-image/TwentyEight';
import TwentyNine from './demo/29.show-and-hide-layers/TwentyNine';
import Thirty from './demo/30.update-a-choropleth-layer/Thirty';
import ThirtyOne from './demo/31.variable-label-placement/ThirtyOne';
import ThirtyTwo from './demo/32.visualize-population-density/ThirtyTwo';
import ThirtyThree from './demo/33.add-a-canvas-source/ThirtyThree';
import ThirtyFour from './demo/34.add-a-video/ThirtyFour';
import ThirtyFive from './demo/35.add-a-wms-source/ThirtyFive';
import ThirtySix from './demo/36.add-a-wms-source/ThirtySix';
import ThirtySeven from './demo/37.create-a-draggable-point/ThirtySeven';
import ThirtyEight from './demo/38.filter-symbols-by-text-input/ThirtyEight';
import ThirtyNine from './demo/39.filter-features-within-map-view/ThirtyNine';
import Fourty from './demo/40.filter-symbols-by-toggling-a-list/Fourty';
import FourtyOne from './demo/41.create-a-hover-effect/FourtyOne';
import FourtyTwo from './demo/42.measure-distance/FourtyTwo';
import FourtyThree from './demo/43.hightlight-features-similar-data/FourtyThree';
import FourtyFour from './demo/44.select-features-around-a-clicked-point/FourtyFour';
import FourtyFive from './demo/45.restrict-map-panning-to-an-area/FourtyFive';
import FourtySix from './demo/46.create-a-time-slider/FourtySix';
import FourtySeven from './demo/47.fit-to-the-bounds-of-a-linestring/FourtySeven';
import FourtyEight from './demo/48.highlight-features-withiin-a-bounding-box/FourtyEight';
import FourtyNine from './demo/49.animate-a-point-along-a-route/FourtyNine';
import 'mapbox-gl/dist/mapbox-gl.css';

mapboxgl.accessToken='pk.eyJ1IjoianRqIiwiYSI6ImNrNGw2YnB6YzBkbWYzbnM4eDI2cGZ3c3cifQ.Q1yjc-bFHp3q6LQ6Whm8lw';
export default class Map extends Component {
    constructor(props){
        super(props);
        this.state = {
            map:null
        }
    }
    componentDidMount(){
        const map = new mapboxgl.Map({
            container:'map',
            // style:'mapbox://styles/mapbox/streets-v9',
            // style:'mapbox://styles/mapbox/light-v10',
            style:'mapbox://styles/mapbox/streets-v11',
            // style: 'mapbox://styles/mapbox/dark-v10',
            center: [117.649, 39.019],
            zoom:17.5,
            pitch:60,
            // bearing: -96,
            antialias: true
        });
        this.setState({
            map:map
        })
    }
    render() {
        const {map} = this.state;
        return (
            <div id="map">
                <Provider value={{map:map,mapboxgl:mapboxgl}}>
                {/* {map && <Two></Two>} */}
                    {/* {map&& <One></One>}
                     {map && <Two></Two>}
                    {map && <Three></Three>} */}
                    {/* {map && <Four></Four>} */}
                    {/* {map && <Five></Five>} */}
                    {/* {map && <Twelve></Twelve>} */}
                    {/* {map && <Thirteen></Thirteen>} */}
                    {/* {map && <Fifteen></Fifteen>} */}
                    {map && <Fourteen></Fourteen>}
                    {/* {map && <Sixteen></Sixteen>} */}
                    {/* {map && <Seventeen></Seventeen>} */}
                    {/* {map && <Eighteen></Eighteen>} */}
                    {/* {map && <Nighteen></Nighteen>} */}
                    {/* {map && <Twenty></Twenty>} */}
                    {/* {map && <TwentyOne></TwentyOne>} */}
                    {/* {map && <TwentyTwo></TwentyTwo>} */}
                    {/* {map && <TwentyThree></TwentyThree>} */}
                    {/* {map && <TwentyFour></TwentyFour>} */}
                    {/* {map && <TwentyFive></TwentyFive>} */}
                    {/* {map && <TwentySix></TwentySix>} */}
                    {/* {map && <TwentySeven></TwentySeven>} */}
                    {/* {map && <TwentyEight></TwentyEight>} */}
                    {/* {map && <TwentyNine></TwentyNine>} */}
                    {/* {map && <Thirty></Thirty>} */}
                    {/* {map && <ThirtyOne></ThirtyOne>} */}
                    {/* {map && <ThirtyTwo></ThirtyTwo>} */}
                    {/* {map && <ThirtyThree></ThirtyThree>} */}
                    {/* {map && <ThirtyFour></ThirtyFour>} */}
                    {/* {map && <ThirtySix></ThirtySix>} */}
                    {/* {map && <ThirtyFive></ThirtyFive>} */}
                    {/* {map && <ThirtySeven></ThirtySeven>} */}
                    {/* {map && <ThirtyEight></ThirtyEight>} */}
                    {/* {map && <ThirtyNine></ThirtyNine>} */}
                    {/* {map && <Fourty></Fourty>} */}
                    {/* {map && <FourtyOne></FourtyOne>} */}
                    {/* {map && <FourtyTwo></FourtyTwo>} */}
                    {/* {map && <FourtyThree></FourtyThree>} */}
                    {/* {map && <FourtyFour></FourtyFour>} */}
                    {/* {map && <FourtyFive></FourtyFive>} */}
                    {/* {map && <FourtySix></FourtySix>} */}
                    {/* {map && <FourtySeven></FourtySeven>} */}
                    {/* {map && <FourtyEight></FourtyEight>} */}
                    {/* {map && <FourtyNine></FourtyNine>} */}

                </Provider>
                    
            </div>
        )
    }
}
