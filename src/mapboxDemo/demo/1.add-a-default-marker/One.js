import React, { Component } from 'react'
import {Consumer} from '@/context/MapContext'
import './one.scss'
const handleConsumer = Cmp => props =>{
    return <Consumer>
        {ctx => {
            return <Cmp {...ctx} {...props}></Cmp>
        }}
    </Consumer>
}
const markerGeojson = {
    type:'Featurecollection',
    features:[{
        geometry:{
            type:'Point',
            coordinates:[117.25,39.2]
        },
        properties:{
            title:'Marker1',
            description:'this is marker 1'
        }
    },
    {
        geometry:{
            type:'Point',
            coordinates:[117.3,39.1]
        },
        properties:{
            title:'Marker2',
            description:'this is marker 2'
        }
    }
]

}
export default class One extends Component {
    render() {
        const Marker= handleConsumer(ShowMarker);
        return (
            <Marker markers = {markerGeojson.features}></Marker>
            // <div>
            //     <Consumer>
            //         {ctx=>{
            //            return <ShowMarker {...ctx}></ShowMarker>
            //         }}
            //     </Consumer>
            // </div>
        )
    }
}
function ShowMarker(props){
    console.log('showMarker',props);
    const {map,mapboxgl,markers} = props;
    markers.forEach((marker)=>{
        const el = createEl(marker);
        new mapboxgl.Marker(el)
        .setLngLat(marker.geometry.coordinates)
        .setPopup(new mapboxgl.Popup({offset:25}).
        setHTML(`<h3>${marker.properties.title}</h3>`+`<p>${marker.properties.description}</p>`))
        .addTo(map);
    })
    // let marker = new mapboxgl.Marker().setLngLat([117.4,39.2]).addTo(map);
    return null
}

function createEl(marker){
    const el = document.createElement('div');
    el.className='marker';
    return el;
}
