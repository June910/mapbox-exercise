import React,{useContext} from 'react';
import {Context} from '@/context/MapContext';
import building from './data/data.json';
export default function Twelve() {
    const {map,mapboxgl} = useContext(Context);
    map.on('load',()=>{
        console.log('3D building map loaded');
        map.addSource("building",{
            type:"geojson",
            data:building
        });
        map.addLayer({
            id:"building-layer",
            type:'fill-extrusion',
            source:"building",
            paint:{
                'fill-extrusion-color': ['get','color'],
                'fill-extrusion-height': ['get','height'],
                'fill-extrusion-base': ['get','base_height'],
                'fill-extrusion-opacity': 0.5
            }
        })
    })
    return null
}
