import React, { useContext } from 'react';
import { Context } from '@/context/MapContext';
export default function Thirteen() {
    const { map, mapboxgl } = useContext(Context);




    map.on('load', () => {
        var layers = map.getStyle().layers;


        var labelLayerId;
        for (var i = 0; i < layers.length; i++) {
            if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
                labelLayerId = layers[i].id;
                break;
            }
        }
        console.log('labelLayerId',labelLayerId);
        console.log('3D building map loaded');
        map.addLayer({
            id: "3d-buildings",
            source: 'composite',
            'source-layer': 'building',
            filter: ["==", 'extrude', 'true'],
            minzoom: 15,
            type: 'fill-extrusion',
            paint: {
                'fill-extrusion-color': '#aaa',
                'fill-extrusion-height': [
                    'interpolate',
                    ['linear'],
                    ['zoom'],
                    15,
                    0,
                    15.05,
                    ['get', 'height']
                ],
                'fill-extrusion-base': [
                    'interpolate',
                    ['linear'],
                    ['zoom'],
                    15, 0, 15.5, ['get', 'min_height']
                ],
                'fill-extrusion-opacity': 0.5
            }
        },labelLayerId)
    })
    return null
}
