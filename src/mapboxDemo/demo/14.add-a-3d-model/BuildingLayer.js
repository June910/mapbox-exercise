import * as THREE from 'three';
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader';
import * as geolib from 'geolib';
import { BufferGeometryUtils } from 'three/examples/jsm/utils/BufferGeometryUtils';
export default class BuildingLayer {
    constructor(paramObj) {
        console.log('paramObj',paramObj);
        this.id = 'custombuilding';
        this.type = 'custom';
        this.renderingMode = '3d';
        // this.buildingGeojson = paramObj.buildingGeojson;
        this.modelOrigin = paramObj.modelOrigin;
        this.modelAltitude = paramObj.modelAltitude;
        this.modelScale = paramObj.modelScale;
        this.modelRotate =paramObj.modelRotate;
        this.modelAsMercatorCoordinate = paramObj.modelAsMercatorCoordinate;
        this.modelTransform = {
            translateX: this.modelAsMercatorCoordinate.x,
            translateY: this.modelAsMercatorCoordinate.y,
            translateZ: this.modelAsMercatorCoordinate.z,
            rotateX: this.modelRotate[0],
            rotateY: this.modelRotate[1],
            rotateZ: this.modelRotate[2],
            scale: this.modelAsMercatorCoordinate.meterInMercatorCoordinateUnits()
        };
        this.buildings = [];
        this.buildingMaterial = new THREE.MeshLambertMaterial({
            color: 0xFF00FF
        });
        this.center = paramObj.modelOrigin;
    }
    getBuilding() {
        // console.log('get builidng');
        fetch('./buildings.json').then((res) => {
            res.json().then(data => {
                this.generateBuilding(data);
            })
        })
        // axios.get('../../../buildings.json').then(data=>{
        //     console.log(data);
        // })
    }
    generateBuilding(data) {
        let features = data.features;
        features.forEach(feature => {
            let coorList = feature.geometry.coordinates[0];
            let properties = feature.properties;
            let relativeCoorList = coorList.map((coorArr) => {
                return this.caculateRelativePosition(coorArr, this.center)
                // return this.caculateRelativePosition(coorArr);

            });
            this.genGeometry(relativeCoorList, properties);
        });
        let mergeGeometry = BufferGeometryUtils.mergeBufferGeometries(this.buildings);

        let mesh = new THREE.Mesh(mergeGeometry, this.buildingMaterial);
        this.scene.add(mesh);

    }
    caculateRelativePosition(start, end) {

        //
        let distance = geolib.getDistance(start, end);
        let bearing = geolib.getRhumbLineBearing(end, start);
        //    let x = start[0] + (distance * Math.cos(bearing * Math.PI / 180));
        //    let y = start[1] + (distance * Math.sin(bearing * Math.PI / 180));
        let y = distance * Math.cos(bearing * Math.PI / 180);
        let x = distance * Math.sin(bearing * Math.PI / 180);
        return [x , y ]
    }
    genGeometry(relativeCoorList, info) {
        let shape = new THREE.Shape();
        for (let i = 0; i <= relativeCoorList.length - 1; i++) {
            let coorArr = relativeCoorList[i];
            if (i == 0) {
                shape.moveTo(coorArr[0], coorArr[1]);
            }
            else {
                shape.lineTo(coorArr[0], coorArr[1]);
            }
        }
        let height = info['层数1'] * 3;
        let geometry = new THREE.ExtrudeBufferGeometry(shape, {
            curveSegments: 1,
            depth: height,
            bevelEnabled: false
        });
        // geometry.rotateX(-Math.PI / 2);
        //    geometry.rotateZ(Math.PI);
        this.buildings.push(geometry);



    }
    onAdd(map, gl) {
        this.camera = new THREE.Camera();
        this.scene = new THREE.Scene();
        const directionLight = new THREE.DirectionalLight(0xffffff);
        directionLight.position.set(0, -70, 100).normalize();
        this.scene.add(directionLight);
        const directionLight2 = new THREE.DirectionalLight(0xffffff);
        directionLight2.position.set(0, 70, 100).normalize();
        this.scene.add(directionLight2);
        this.map = map;
        this.renderer = new THREE.WebGLRenderer({
            canvas: map.getCanvas(),
            context: gl,
            antialias: true
        });
        this.renderer.autoClear = false;
        this.getBuilding();
    }
    render(gl, matrix){
        const rotationX = new THREE.Matrix4().makeRotationAxis(
            new THREE.Vector3(1, 0, 0),
            this.modelTransform.rotateX
        );
        const rotationY = new THREE.Matrix4().makeRotationAxis(
            new THREE.Vector3(0, 1, 0),
            this.modelTransform.rotateY
        );
        const rotationZ = new THREE.Matrix4().makeRotationAxis(
            new THREE.Vector3(0, 0, 1),
            this.modelTransform.rotateZ
        );
        const m = new THREE.Matrix4().fromArray(matrix);
        const l = new THREE.Matrix4().makeTranslation(
            this.modelTransform.translateX,
            this.modelTransform.translateY,
            this.modelTransform.translateZ,
        )
            .scale(
                new THREE.Vector3(this.modelTransform.scale,
                    -this.modelTransform.scale,
                    this.modelTransform.scale)
            )
            .multiply(rotationX)
            .multiply(rotationY)
            .multiply(rotationZ);
        this.camera.projectionMatrix = m.multiply(l);
        this.renderer.state.reset();
        this.renderer.render(this.scene, this.camera);
        this.map.triggerRepaint();
    }

}