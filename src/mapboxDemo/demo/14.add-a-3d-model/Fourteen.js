import React, { useContext } from 'react';
import { Context } from '@/context/MapContext';
import * as THREE from 'three';
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader';
import BuildingLayer from './BuildingLayer';
export default function Fourteen() {
    const { map, mapboxgl } = useContext(Context);
    const modelOrigin = [117.649, 39.019];
    const modelAltitude = 0;
    const modelRotate = [0, 0, 0];
    const modelScale = 5.41843220338983e-8;
    const modelAsMercatorCoordinate = mapboxgl.MercatorCoordinate.fromLngLat(modelOrigin, modelAltitude);
    console.log(modelAsMercatorCoordinate.meterInMercatorCoordinateUnits());
    const modelTransform = {
        translateX: modelAsMercatorCoordinate.x,
        translateY: modelAsMercatorCoordinate.y,
        translateZ: modelAsMercatorCoordinate.z,
        rotateX: modelRotate[0],
        rotateY: modelRotate[1],
        rotateZ: modelRotate[2],
        // scale: modelAsMercatorCoordinate.meterInMercatorCoordinateUnits()
        scale:4.215333273821309e-5
    }
    const customLayer = {
        id: '3d-model',
        type: 'custom',
        renderingMode: '3d',
        onAdd: function (map, gl) {
            console.log(this);
            this.camera = new THREE.Camera();
            this.scene = new THREE.Scene();
            const directionLight = new THREE.DirectionalLight(0xffffff);
            directionLight.position.set(0, -70, 100).normalize();
            this.scene.add(directionLight);
            const directionLight2 = new THREE.DirectionalLight(0xffffff);
            directionLight2.position.set(0, 70, 100).normalize();
            this.scene.add(directionLight2);
            const loader = new GLTFLoader();
            loader.load(
                'https://docs.mapbox.com/mapbox-gl-js/assets/34M_17/34M_17.gltf',
                function (gltf) {
                    console.log('gltf',gltf);
                    console.log('this.scene',this.scene);
                    this.scene.add(gltf.scene);
                }.bind(this)
            );
            this.map = map;
            this.renderer = new THREE.WebGLRenderer({
                canvas: map.getCanvas(),
                context: gl,
                antialias: true
            });
            this.renderer.autoClear = false;
        },
        render: function (gl, matrix) {
            console.log(matrix);
            const rotationX = new THREE.Matrix4().makeRotationAxis(
                new THREE.Vector3(1, 0, 0),
                modelTransform.rotateX
            );
            const rotationY = new THREE.Matrix4().makeRotationAxis(
                new THREE.Vector3(0, 1, 0),
                modelTransform.rotateY
            );
            const rotationZ = new THREE.Matrix4().makeRotationAxis(
                new THREE.Vector3(0, 0, 1),
                modelTransform.rotateZ
            );
            const m = new THREE.Matrix4().fromArray(matrix);
            const l = new THREE.Matrix4().makeTranslation(
                modelTransform.translateX,
                modelTransform.translateY,
                modelTransform.translateZ,
            )
                .scale(
                    new THREE.Vector3(modelTransform.scale,
                        -modelTransform.scale,
                        modelTransform.scale)
                )
                .multiply(rotationX)
                .multiply(rotationY)
                .multiply(rotationZ);
            this.camera.projectionMatrix = m.multiply(l);
            this.renderer.state.reset();
            this.renderer.render(this.scene, this.camera);
            this.map.triggerRepaint();

        }
    }
    const buildingLayer = new BuildingLayer({
        modelOrigin,
        modelAltitude,
        modelRotate,
        modelScale,
        modelAsMercatorCoordinate
    });
    map.on('style.load',function(){

        map.addLayer(buildingLayer,'waterway-label');
    });
    const fire = (e)=>{
        console.log(e);
    }  
    map.on('click',fire);
 

    return null
}
