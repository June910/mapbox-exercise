import React,{useContext,useState,useEffect} from 'react';
import {Context} from '@/context/MapContext.js'
import './filteen.scss';

export default function Fifteen() {
    const [value,setValue] = useState('100');
    const {map, mapboxgl} = useContext(Context);
    useEffect(() => {
        //这里的内容只会执行一次
        map.on('load',function(){
            map.addSource('chicago',{
                type:'raster',
                url:'mapbox://mapbox.u8yyzaor'
            });
            console.log(parseInt(value,10) / 100);
            map.addLayer({
                id:'chicago',
                source:'chicago',
                type:'raster'
            })
        });
        return () => {
            
        }
    }, [])
    const change = (e)=>{
        // console.log(e.target.value);
        setValue(e.target.value);
        map.setPaintProperty('chicago','raster-opacity',
            parseInt(e.target.value,10)/100
        )
    }

    return (
        <div className = 'map-overlay top'>
            <label >Layer opacity:{value}
                <span id="slider-value"></span>
            </label>
            <input 
                id="slider"
                type="range"
                min="0"
                max="100"
                setp="0"
                value={value}
                onChange={change}
            ></input>
        </div>
    )
}
