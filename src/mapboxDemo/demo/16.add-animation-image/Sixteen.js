import React, { useState, useContext, useEffect } from 'react';
import { Context } from '@/context/MapContext.js';
import './sixteen.css';
export default function Sixteen() {
    const { map, mapboxgl } = useContext(Context);
    // const [currentImage, setCurrentImage] = useState(0);
    let currentImage = 0;
    const frameCount = 5;
    map.on('load', () => {
        map.addSource("radar", {
            type: 'image',
            url: getPath(),
            coordinates: [
                [-80.425, 46.437],
                [-71.516, 46.437],
                [-71.516, 37.936],
                [-80.425, 37.936]
            ]
        });
        map.addLayer({
            id: "radar-layer",
            type: "raster",
            source: "radar",
            paint: {
                "raster-fade-duration": 0
            }
        })
    })
    const getPath = () => {
        return "https://docs.mapbox.com/mapbox-gl-js/assets/radar" + currentImage + ".gif";
    }
    setInterval(()=>{
        currentImage = (currentImage + 1) % frameCount;
        map.getSource('radar')&&map.getSource('radar').updateImage({url:getPath()});
    },1000);
    console.log('组件');


    return null
}


