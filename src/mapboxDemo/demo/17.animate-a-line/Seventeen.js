import React, { useContext } from 'react'
import { Context } from '@/context/MapContext.js';
import './seventeen.scss'
export default function Seventeen() {
    const { map, mapboxgl } = useContext(Context);
    let isAnimation = false;
    const sppedFactor = 2;
    let animation = null;
    let x = 0;
    let geojson = {
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [[0, 0]]
                }
            }
        ]
    };
    map.on('load', () => {
        map.addSource('line', {
            type: 'geojson',
            data:geojson
        });
        map.addLayer({
            id:'line-animation',
            type:'line',
            source:'line',
            layout:{
                'line-cap':'round',
                'line-join':'round'
            },
            paint:{
                'line-color':'#ed6498',
                'line-width':5,
                'line-opacity':0.8
            }
        })
    })
    const toggleAnimate = ()=>{
        console.log('执行animate按钮');
        isAnimation = !isAnimation;
        if(isAnimation){
            //开始执行动画
            animateLine()
        }else{
            cancelAnimationFrame(animation);
        }
    }
    const animateLine = ()=>{
        if(x<=360){
        x = x + sppedFactor;
        let y = Math.sin((x * Math.PI) / 90) * 40;
        geojson.features[0].geometry.coordinates.push([x,y]);
        }else{
            x = 0;
            geojson.features[0].geometry.coordinates=[];
        }

        map.getSource('line').setData(geojson);
        animation = requestAnimationFrame(animateLine);
    }
    return (
        <div id="animate-button">
            <button onClick={toggleAnimate}>{isAnimation?'pause':'play'}</button>

        </div>
    )
}
