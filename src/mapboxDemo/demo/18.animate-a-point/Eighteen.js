import React,{useContext} from 'react'
import { Context } from '@/context/MapContext.js';


export default function Eighteen() {
    const {map,mpaboxgl} = useContext(Context);
    const point = (angle)=>{
        return {
            type:'Point',
            coordinates:[
                Math.cos(angle)*radius,
                Math.sin(angle)*radius,
            ]
        }
    }
    const radius = 20;
    map.on('load',()=>{
        map.addSource('point',{
            type:'geojson',
            data:point(0)
        });
        map.addLayer({
            id:'point',
            type:'circle',
            source:'point',
            paint:{
                'circle-radius':10,
                'circle-color':'#007cbf'
            }
        });
        animateMarker();
    });
    function animateMarker(timestamp){
        map.getSource('point').setData(point(timestamp/1000));
        requestAnimationFrame(animateMarker);
    }

    return (
        <div>
            
        </div>
    )
}
