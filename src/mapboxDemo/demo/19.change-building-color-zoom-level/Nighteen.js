import React,{useContext} from 'react'
import { Context } from '@/context/MapContext.js';
import './nighteen.scss'

export default function Nighteen() {
    const {map,mpaboxgl} = useContext(Context);
    map.on('load',()=>{
        map.setPaintProperty('building','fill-color',[
            'interpolate',
            ['exponential',0.5],
            ['zoom'],
            15,
            '#e2714b',
            22,
            '#eee695'
        ]);
        map.setPaintProperty('building','fill-opacity',[
            'interpolate',
            ['exponential',0.5],
            ['zoom'],
            15,
            0,
            22,
            1
        ])
    })
    function zoom(){
        map.zoomTo(19,{duration:9000})
    }
    return (
        <div id="btn">
            <button onClick={zoom}>zoom to change builidng color</button>
        </div>
    )
}
