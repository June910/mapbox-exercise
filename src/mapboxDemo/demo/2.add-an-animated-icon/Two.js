import React from 'react'
import {Context} from '@/context/MapContext'
import {useContext} from 'react'


// addImage(id,image,options)
// 图像可以用于icon-image,backgroud-pattern fill-pattern和line-pattern
// 其中image可以是HTLImageElement huozhe ImageData(具有width，height，data属性的对象)
// 如果是HTMLImage 则应用为：
// map.loadImage(url,function(err,image){
//     if(err){
//         throw err;
//     }else{
//         map.addImage('image',Image)
//     }
// })
export default function Two() {
    const {map,mapboxgl} = useContext(Context);
    // const plusingDot = {
    //     width:size,
    //     height:size,
    //     data:new Uint8Array(size*size*4),
    //     onAdd:function(){
    //         console.log('onadd');
    //         const canvas = document.createElement('canvas');
    //         canvas.width = this.width;
    //         canvas.height = this.height;
    //         this.context = canvas.getContext('2d');
    //     },
    //     render:function(){
    //         console.log('render');
    //         const duration = 1000;
    //         let t = (performance.now()%duration)/duration;
    //         const radius = (size/2)*0.3;
    //         let outerRadius = (size/2)*0.7*t+radius;
    
    //         //开始画外环
    //         this.context.clearRect(0,0,this.width,this.height);
    //         this.context.beginPath();
    //         this.context.arc(this.width/2,this.height/2,outerRadius,0,Math.PI*2);
    //         this.context.fillStyle = 'rgba(255,200,200,'+(1-t)+')';
    //         this.context.fill();
    
    //         //开始画内环
    //         this.context.beginPath();
    //         this.context.arc(this.width/2,this.height/2,radius,0,Math.PI * 2);
    //         this.context.fillStyle = 'rgba(255,100,100,1)';
    //         this.context.strokeStyle = 'white';
    //         this.context.lineWidth = 2 + 4 * (1-t);
    //         this.context.fill();
    //         this.context.stroke();
    //         this.data = this.context.getImageData(
    //             0,
    //             0,
    //             this.width,
    //             this.height
    //         ).data;
    //         console.log('data',this.data);
    //         map.triggerRepaint();
    //         return true;
    //     }
    // };
    let icon = new Icon(map,100);
    map.on('load',function(){
        console.log('map-loaded');
        map.addImage('plusing-dot',icon,{pixelRadio:2});
        map.addSource('points',{
            type:'geojson',
            data:{
                'type':'FeatureCollection',
                'features':[{
                    'type':'Feature',
                    'geometry':{
                        'type':'Point',
                        'coordinates':[117,39]
                    }
                },
                {
                    'type':'Feature',
                    'geometry':{
                        'type':'Point',
                        'coordinates':[117.1,39.1]
                    }
                },
                {
                    'type':'Feature',
                    'geometry':{
                        'type':'Point',
                        'coordinates':[117.2,39.2]
                    }
                },
                {
                    'type':'Feature',
                    'geometry':{
                        'type':'Point',
                        'coordinates':[117.3,39.3]
                    }
                },
                {
                    'type':'Feature',
                    'geometry':{
                        'type':'Point',
                        'coordinates':[117.4,39.4]
                    }
                },
                {
                    'type':'Feature',
                    'geometry':{
                        'type':'Point',
                        'coordinates':[117.5,39.5]
                    }
                }]
            }
        });
        map.addLayer({
            id:'points',
            type:'symbol',
            source:'points',
            layout:{
                'icon-image':'plusing-dot'
            }
        })
    })
    return null
}

//自定义一个icon
class Icon {
    constructor(map,size){
        this.map = map;
        this.size = size;
        this.height = size;
        this.width = size;
        // this.size*this.size*4
        // this.data = new Uint8Array(1000);
        this.data = new Array(this.size*this.size*4);//this.data初始化时，必须和getImageData中data的数量一致
    }
    onAdd(){
        const canvas = document.createElement('canvas');
        canvas.width = this.size;
        canvas.height = this.size;
        this.context = canvas.getContext('2d');
    }
    render(gl,matrix){
        console.log('matrix',matrix);
        const duration = 1000;
        let t = (performance.now()%duration)/duration;
        const radius = (this.size/2)*0.3;
        let outerRadius = (this.size/2)*0.7*t+radius;
        //开始画外环
        this.context.clearRect(0,0,this.width,this.height);
        this.context.beginPath();
        this.context.arc(this.width/2,this.height/2,outerRadius,0,Math.PI*2);
        this.context.fillStyle = 'rgba(255,200,200,'+(1-t)+')';
        this.context.fill();

        //开始画内环
        this.context.beginPath();
        this.context.arc(this.width/2,this.height/2,radius,0,Math.PI*2);
        this.context.fillStyle = 'rgba(255,100,100,1)';
        this.context.strokeStyle = 'white';
        this.context.lineWdith = 2+4*(1-t);
        this.context.fill();
        this.context.stroke();
        this.data = this.context.getImageData(
            0,
            0,
            this.width,
            this.height
        ).data;//getImageData的data中是rgba  数组中每四个为一组 分别为rgba  size*size*4
        console.log(this.data);
        this.map.triggerRepaint();//keep the map repainting  触发一个显示框的渲染
        return true;//let the map konw the image was updated

    }
}

