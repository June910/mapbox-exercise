import React,{useContext} from 'react'
import { Context } from '@/context/MapContext.js';
import mapboxGl from 'mapbox-gl';

export default function Twenty() {
    const {map,mpaboxgl} = useContext(Context);
    map.on('load',()=>{
        map.addSource('earthquakes',{
            type:'geojson',
            data:'https://docs.mapbox.com/mapbox-gl-js/assets/earthquakes.geojson',
            cluster:true,
            clusterMaxZoom:14,
            clusterRadius: 50
        });
        map.addLayer({
            id:'clusters',
            type:'circle',
            source:'earthquakes',
            // filter:['has','point_count'],
            filter:['==','cluster',true],
            paint:{
                'circle-color':[
                    'step',
                    ['get','point_count'],
                    "#51bbd6",
                    100,
                    '#f1f075',
                    750,
                    '#f28cb1'
                ],
                'circle-radius':[
                    'step',
                    ['get','point_count'],
                    20,100,30,750,40
                ]
            }

        });
        map.addLayer({
            id:'cluster-count',
            type:'symbol',
            source:'earthquakes',
            filter:['has','point_count'],
            layout:{
                'text-field':'{point_count_abbreviated}',
                'text-font':['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                'text-size':12
            }
        })
        map.addLayer({
            id:'unclustered-point',
            type:'circle',
            source:'earthquakes',
            filter:['!',['has','point_count']],
            paint:{
                'circle-color':'#11b4da',
                'circle-radius':4,
                'circle-stroke-width':1,
                'circle-stroke-color':'#fff'
            }
        });

    });
    map.on('mouseenter','clusters',()=>{
        map.getCanvas().style.cursor="pointer"
    });
    map.on('mouseleave','clusters',()=>{
        map.getCanvas().style.cursor = '';
    });
    map.on('click','unclustered-point',(e)=>{
        let feature = e.features[0];
        let coordinates = feature.geometry.coordinates.slice();
        let mag = feature.properties.mag;
        let tsuami = feature.properties.tsunami === 1? 'yes': 'no';
        new mapboxGl.Popup().setLngLat(coordinates).setHTML(
            'magnitude:' + mag + '<br>Was there a tsunami?:' + tsuami
        )
        .addTo(map);
    });

    map.on('click','clusters',(e)=>{
        let features = map.queryRenderedFeatures(e.point,{
            layers:['clusters']
        });
        console.log(features);
        let clustedId = features[0].properties.cluster_id;
        map.getSource('earthquakes').getClusterExpansionZoom(clustedId,
            function(err,zoom){
                if (err) return;
                map.easeTo({
                    center: features[0].geometry.coordinates,
                    zoom:zoom
                });
            }
        )
    });

    return null
}