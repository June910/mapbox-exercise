import React,{useContext} from 'react'
import { Context } from '@/context/MapContext.js';

export default function TwentyTwo() {
    const {map,mpaboxgl} = useContext(Context);
    map.on('load',()=>{
        map.addSource('label',{
            type:'geojson',
            data:'https://docs.mapbox.com/mapbox-gl-js/assets/boise.geojson'
        });
        map.addLayer({
            id:'off-leash-areas',
            type:'symbol',
            source:'label',
            layout:{
                'icon-image':'dog-park-11',
                'text-field':[
                    'format',
                    ['upcase',['get','FacilityName']],
                    {'font-scale': 0.8},
                    '\n',
                    {},
                    ['downcase',['get','Comments']],
                    {'font-scale': 0.4}
                ],
                'text-font':['Open Sans Semibold', 'Arial Unicode MS Bold'],
                'text-size':40,
                'text-offset':[0, 0.6],
                'text-anchor':'top'
            }
        })
    })

    return null
}