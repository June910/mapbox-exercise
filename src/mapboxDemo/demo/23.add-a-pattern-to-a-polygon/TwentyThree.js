import React, { useContext } from 'react'
import { Context } from '@/context/MapContext.js';

export default function TwentyThree() {
    const { map, mpaboxgl } = useContext(Context);
    const data = {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'Polygon',
                'coordinates': [
                    [
                        [-30, -25],
                        [-30, 35],
                        [30, 35],
                        [30, -25],
                        [-30, -25]
                    ]
                ]
            }
        }
    map.on('load', async() => {
        map.addSource('source',{
            type:'geojson',
            data
        });
        let img = await loadRemoteImage('https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Cat_silhouette.svg/64px-Cat_silhouette.svg.png');
        map.addImage('pattern',img);
        map.addLayer({
            id:'pattern-layer',
            type:'fill',
            source:'source',
            paint:{
                'fill-pattern':'pattern'
            }
        });
    })
    const loadRemoteImage = (url)=>{
        return new Promise((resolve,reject)=>{
            map.loadImage(url,(err,img)=>{
                if(err) reject()
                resolve(img)
            })
        })
    }

    return null
}