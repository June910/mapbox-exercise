import React, { useContext } from 'react'
import { Context } from '@/context/MapContext.js';

export default function TwentyThree() {
    const { map, mpaboxgl } = useContext(Context);
    map.on('load',()=>{
        map.addSource('population',{
            type:'vector',
            url:'mapbox://examples.8fgz4egr'
        });
        map.addLayer({
            id:'population',
            type:'circle',
            source:'population',
            'source-layer':'sf2010',
            paint:{
                // 'circle-radius':180,
                'circle-radius':{
                    base:1.75,
                    stops:[
                        [12,2],[22,50]
                    ]
                },
                'circle-color':[
                    'match',
                    ['get','ethnicity'],
                    'White','#fbb03b',
                    'Black','#223b53',
                    'Hispanic','#e55e5e',
                    'Asian','#3bb2d0',
                    '#ccc'
                ]
            }

            
        })
    })

    return null
}