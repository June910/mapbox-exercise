import React, { useContext } from 'react'
import { Context } from '@/context/MapContext.js';

export default function TwentyEight() {
    const { map, mpaboxgl } = useContext(Context);
    const geojson = {
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [
                        -77.03238901390978,
                        38.913188059745586
                    ]
                },
                'properties': {
                    'title': 'Washington DC',
                    'icon': 'monument'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-79.9959, 40.4406]
                },
                'properties': {
                    'title': 'Pittsburgh',
                    'icon': 'bridges'
                }
            },
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-76.2859, 36.8508]
                },
                'properties': {
                    'title': 'Norfolk',
                    'icon': 'harbor'
                }
            }
        ]
    }

    map.on('load', () => {
        map.addSource('point', {
            type: 'geojson',
            data: geojson
        });
        map.addLayer({
            id: 'points',
            type: 'symbol',
            source: 'point',
            layout: {
                'icon-image': [
                    'coalesce',
                    ['image',['concat',['get','icon'],'-15']],
                    ['image','marker-15']
                ],
                'text-field': [
                    'get', 'title'
                ],
                'text-font': ['Open Sans Semibold'],
                'text-offset': [0, 0.6],
                'text-anchor': 'top'
            }
        });
    })

    return null
}