import React, { useContext,useState,useEffect } from 'react'
import { Context } from '@/context/MapContext.js';
import './twentynine.scss';
export default function TwentyNine() {
    const { map, mpaboxgl } = useContext(Context);
    let [contours,setContours] = useState('visible');
    let [museum,setMuseum] = useState('visible');
    const handleConClick = ()=>{
        console.log('contours',contours);
        (contours === 'visible')? setContours('none'): setContours('visible')
    };
    const handleMuseClick = ()=>{
        (museum === 'visible')? setMuseum('none'): setMuseum('visible')

    }
    map.on('load',()=>{
        console.log('map load');
        map.addSource('museums',{
            type:'vector',
            url:'mapbox://mapbox.2opop9hr'
        });
        map.addLayer({
            id:'museums',
            type:'circle',
            source:'museums',
            layout:{
                visibility:museum
            },
            paint:{
                'circle-radius':8,
                'circle-color':'rgba(55,148,179,1)'
            },
            'source-layer':'museum-cusco'
        });
        map.addSource('contours',{
            type:'vector',
            url:'mapbox://mapbox.mapbox-terrain-v2'
        });
        map.addLayer({
            id:'contours',
            type:'line',
            source:'contours',
            'source-layer':'contour',
            layout:{
                visibility:contours,
                'line-join':'round',
                'line-cap':'round'
            },
            paint:{
                'line-color':'#877b59',
                'line-width':1
            }
        })
    })

    useEffect(() => {
        map.getLayer('museums') && map.setLayoutProperty('museums','visibility',museum)
    }, [museum])
    useEffect(() => {
        map.getLayer('contours') && map.setLayoutProperty('contours','visibility',contours)
    }, [contours])


    return (
        <div id="control">
            <ul>
                <li>
                    <button onClick={handleConClick}>contours</button>
                </li>
                <li>
                    <button onClick={handleMuseClick}>museums</button>
                </li>
            </ul>
        </div>
    )
}