import React,{useContext} from 'react'
import {Context,Consumer} from '@/context/MapContext.js'
export default function Three() {
    const {map,mapboxgl} = useContext(Context);
    map.on('load',function(){
        console.log('mapLoad');
        let gradientIcon = new GradintIcon(64);
        map.addImage('gradient-icon',gradientIcon);
        map.addSource('point',{
            type:'geojson',
            data:{
                type:'FeatureCollection',
                features:[
                    {
                        type:'feature',
                        geometry:{
                            type:'Point',
                            coordinates:[117.25,39.06]
                        }
                    }
                ]
            }
        });
        map.addLayer({
            id:'gradientlayer',
            type:'symbol',
            source:'point',
            layout:{
                'icon-image':'gradient-icon'
            }

        })
    })
    return null
}

class GradintIcon{
    constructor(size){
        this.bytePerPixel = 4
        this.size = size;
        this.width = size;
        this.height = size;
        this.data = new Uint8Array(this.size*this.size*4);
        this.GeneratePixel();
    }
    GeneratePixel(){
        for(let i = 0;i<this.size;i++){
            for(let j=0;j<this.size;j++){
               let offset =  (j+i*this.size)*this.bytePerPixel;
               this.data[offset+0] = i/this.size*255;//red
               this.data[offset+1] = i/this.size*255;//green
               this.data[offset+2] = 128;
               this.data[offset+3] = 255;
            }
        }
    }
}