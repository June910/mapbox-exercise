import React, { useContext, useState,memo } from 'react'
import { Context } from '@/context/MapContext.js';
import './thirty.scss';
const options = [
    {
        property: 'population', stops: [
            [0, '#F2F12D'],
            [50000, '#EED322'],
            [750000, '#E6B71E'],
            [1000000, '#DA9C20'],
            [2500000, '#CA8323'],
            [5000000, '#B86B25'],
            [7500000, '#A25626'],
            [10000000, '#8B4225'],
            [25000000, '#723122']
        ]
    },
    {
        property: 'population', stops: [
            [0, '#F2F12D'],
            [100, '#EED322'],
            [1000, '#E6B71E'],
            [5000, '#DA9C20'],
            [10000, '#CA8323'],
            [50000, '#B86B25'],
            [100000, '#A25626'],
            [500000, '#8B4225'],
            [1000000, '#723122']
        ]
    }
]
export default function Thirty() {
    const { map, mpaboxgl } = useContext(Context);

    // const [activeOption,setActiveOption] = useState({...options[1]})
    const [index,setIndex] = useState(0);
    map.on('load', () => {
        map.addSource('population', {
            type: 'vector',
            url: 'mapbox://mapbox.660ui7x6'
        });
        map.addLayer({
            id: 'state-population',
            source: 'population',
            'source-layer': 'state_county_population_2014_cen',
            maxzoom: 4,
            type: 'fill',
            filter: ['==', 'isState', true],
            paint: {
                'fill-color': options[0],
                'fill-opacity': 0.75
            },
        }, 'waterway-label');
        map.addLayer({
            id: 'county-population',
            source: 'population',
            'source-layer': 'state_county_population_2014_cen',
            minzoom:4,
            type: 'fill',
            filter: ['==', 'isCounty', true],
            paint:{
                'fill-color':options[1],
                'fill-opacity':0.75
            }
        },'waterway-label')
    })
    map.on('zoom',()=>{
        let zoom = map.getZoom();
        // zoom>4 ? setActiveOption({...options[1]}) : setActiveOption({...options[0]})
        zoom>4 ? setIndex(1) : setIndex(0)
    })
    // useEffect(() => {
    //     effect
    // }, [activeOption])
    return (
        <Legend activeIndex = {index} ></Legend>
    )
}

const Legend=memo((props)=> {
    console.log('legend',props);
    let index = props.activeIndex;
    let activeOption = options[index].stops;
    console.log(activeOption);
    return (
        <div id='legend'>
            <ul>
            {activeOption.map((item,index)=>
                <li key={index}>{item[0]}</li>
            )}
            </ul>



        </div>
    )
    
})