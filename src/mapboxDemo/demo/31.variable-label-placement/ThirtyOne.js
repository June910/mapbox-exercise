import React, { useContext, useState, memo } from 'react'
import { Context } from '@/context/MapContext.js';
export default function ThirtyOne() {

    const { map, mpaboxgl } = useContext(Context);
    const places = {
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'properties': {
                    'description': "Ford's Theater",
                    'icon': 'theatre'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.038659, 38.931567]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'description': 'The Gaslight',
                    'icon': 'theatre'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.003168, 38.894651]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'description': "Horrible Harry's",
                    'icon': 'bar'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.090372, 38.881189]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'description': 'Bike Party',
                    'icon': 'bicycle'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.052477, 38.943951]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'description': 'Rockabilly Rockstars',
                    'icon': 'music'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.031706, 38.914581]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'description': 'District Drum Tribe',
                    'icon': 'music'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.020945, 38.878241]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'description': 'Motown Memories',
                    'icon': 'music'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.007481, 38.876516]
                }
            }
        ]
    };
    map.on('load',()=>{
        map.addSource('place',{
            type:'geojson',
            data:places
        });
        map.addLayer({
            id:'place',
            type:'symbol',
            source:'place',
            layout:{
                'text-field':['get','description'],
                'text-variable-anchor':['top','bottom','left','right'],
                'text-radial-offset':0.5,
                'text-justify':'auto',
                'icon-image':['concat',['get','icon'],'-15']
            }
        });
        map.rotateTo(180,{duration:10000});

    })
    return null
}
