import React, { useContext, useState, memo } from 'react'
import { Context } from '@/context/MapContext.js';
export default function ThirTwo() {

    const { map, mpaboxgl } = useContext(Context);

    map.on('load',()=>{
        map.addSource('rwanda-provinces',{
            type:'geojson',
            data:'https://docs.mapbox.com/mapbox-gl-js/assets/rwanda-provinces.geojson'
        });
        map.addLayer({
            id:'rwanda-provinces',
            type:'fill',
            source:'rwanda-provinces',
            paint:{
                'fill-color':[
                    'let',
                    'density',
                    ['/',['get','population'],['get','sq-km']],
                    [
                        'interpolate',
                        ['linear'],
                        ['zoom'],
                        8,
                        [
                            'interpolate',
                            ['linear'],
                            ['var','density'],
                            274,
                            // ['to-color','#edf8e9'],
                            '#edf8e9',
                            1551,
                            '#006d2c'
                            // ['to-color','#006d2c']

                        ],
                        10,
                        [
                            'interpolate',
                            ['linear'],
                            ['var','density'],
                            274,
                            '#eff3ff',
                            // ['to-color','#eff3ff'],
                            1551,
                            '#08519c'
                            // ['to-color','#08519c']
                        ]
                    ]
                ],
                'fill-opacity':0.7
            }
        });
        map.jumpTo({
            center:[30.0222, -1.9596],
            zoom:8
        });

    })
    return null
}
