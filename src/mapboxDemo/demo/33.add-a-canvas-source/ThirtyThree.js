import React, { useContext } from 'react'
import { Context } from '@/context/MapContext.js';

export default function ThirtyThree() {
    const { map } = useContext(Context);
    const canvas = document.createElement('canvas');
    canvas.setAttribute('width',800);
    canvas.setAttribute('height',800);
    const context = canvas.getContext('2d');
    const radius = 20;
    function Circle(x, y, dx, dy, radius, color) {
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
        this.radius = radius;
        this.color = color;

        this.draw = function () {
            context.beginPath();
            context.strokeStyle = this.color;
            context.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
            context.stroke();
        };
        this.update = function () {
            if ((this.x + this.radius) > 800 || this.x - this.radius < 0) {
                this.dx = -this.dx;
            }
            if ((this.y + this.radius) > 800 || this.y - this.radius < 0) {
                this.dy = -this.dy;
            }
            this.x += this.dx;
            this.y += this.dy;
            this.draw();

        }
    }
    let circles = [];
    for (let i = 0; i < 5; i++) {
        var color =
            '#' +
            (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);
        var x = Math.random() * (800 - radius * 2) + radius;
        var y = Math.random() * (800 - radius * 2) + radius;

        var dx = (Math.random() - 0.5) * 2;
        var dy = (Math.random() - 0.5) * 2;
        let circle = new Circle(x, y, dx, dy, radius, color);
        circles.push(circle);
    }
    function animate() {
        context.clearRect(0, 0, 800, 800);
        circles.forEach(circle => {
            circle.update();
        })
        requestAnimationFrame(animate);
    }
    animate();
    map.on('load', () => {
        map.addSource('canvas-source', {
            type: 'canvas',
            canvas,
            coordinates: [
                [91.4461, 21.5006],
                [100.3541, 21.5006],
                [100.3541, 13.9706],
                [91.4461, 13.9706]
            ]
        });
        map.addLayer({
            id:"canvas-layer",
            type: 'raster',
            source: 'canvas-source'
        });
    })
    return null
}
