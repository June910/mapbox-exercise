import React, { useContext, useState, memo } from 'react'
import { Context } from '@/context/MapContext.js';
import axios from 'axios';
export default function ThirTwo() {
    const url = 'https://docs.mapbox.com/mapbox-gl-js/assets/hike.geojson';
    const { map, mpaboxgl } = useContext(Context);
    const getData = (url)=>{
        return axios.get(url);
    }
    map.on('load',async()=>{
        // features[0].geometry.coordinates;
        // let coorArr = [];
        let data = await getData(url);
        let geojsonLine = data.data;
        let coorList = geojsonLine.features[0].geometry.coordinates;
        map.jumpTo({ 'center': coorList[0], 'zoom': 14 });
        geojsonLine.features[0].geometry.coordinates=[];
        map.addSource('trace',{
            type:'geojson',
            data:geojsonLine
        });
        map.addLayer({
            id:'trace-layer',
            type:'line',
            source:'trace',
            layout:{
                'line-join':'round'
            },
            paint:{
                'line-width':3,
                'line-color':'yellow',
                'line-opacity':0.75,
            }
        });
        let i =0;
        const timer = setInterval(()=>{
            if(i<=coorList.length){
                geojsonLine.features[0].geometry.coordinates.push(coorList[i]);
                map.getSource('trace').setData(
                    geojsonLine
                )
                map.panTo(coorList[i]);
                i++;
            }else{
                clearInterval(timer);
            }
            // coorArr.push(coorList[i]);

        },10)
    })
    return null
}
