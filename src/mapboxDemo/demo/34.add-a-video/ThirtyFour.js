import React, { useContext } from 'react';
import { Context } from '@/context/MapContext.js';


export default function ThirtyFour() {
    const { map, mapboxgl } = useContext(Context);
    map.on('load', () => {
        map.addSource('video', {
            type: 'video',
            urls: [
                'https://static-assets.mapbox.com/mapbox-gl-js/drone.mp4',
                'https://static-assets.mapbox.com/mapbox-gl-js/drone.webm'
            ],
            coordinates: [
                [-122.51596391201019, 37.56238816766053],
                [-122.51467645168304, 37.56410183312965],
                [-122.51309394836426, 37.563391708549425],
                [-122.51423120498657, 37.56161849366671]
            ]

        });
        map.addLayer({
            id:'video-layer',
            source:'video',
            type:'raster',
            
        });
        let playingVideo = true;
        map.on('click','video-layer',()=>{
            playingVideo = !playingVideo;
            if(playingVideo)  map.getSource('video').play();
            else map.getSource('video').pause();
        })
    })
    return null
}
