import React, {useContext} from 'react'
import { Context } from '@/context/MapContext.js';

export default function ThirtyFive() {
    const {map, mapboxgl} = useContext(Context);
    map.on('load', () => {
        map.addSource('raster-tiles', {
            type: 'raster',
            tiles: [
                // 'https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg'
                'http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}'
            ],
            tileSize:256
        });
        map.addLayer({
            id:'raster-tile-layer',
            type:'raster',
            source:'raster-tiles'
        });

    });
    return null
}
