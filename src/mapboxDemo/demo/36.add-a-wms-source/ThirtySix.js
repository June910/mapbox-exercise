import React, { useContext } from 'react';
import { Context } from '@/context/MapContext.js';


export default function ThirtySix() {
    const { map, mapboxgl } = useContext(Context);
    map.on('load', () => {
        map.addSource('wms-source', {
            type: 'raster',
            tiles: [
                // 'https://img.nj.gov/imagerywms/Natural2015?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=Natural2015'
                'http://192.168.1.225:8080/geoserver/yd/wms?service=WMS&version=1.1.0&request=GetMap&layers=yd%3Atdlyxz&bbox={bbox-epsg-3857}&width=512&height=512&srs=EPSG%3A3857&format=image/png&TRANSPARENT=TRUE'
                // 'http://192.168.1.225:8080/geoserver/hd/wms?service=WMS&version=1.1.0&bbox={bbox-epsg-4326}&request=GetMap&layers=hd%3Ahdlx&width=256&height=256&srs=EPSG%3A4326&format=image/png'
            ],
            tileSize:256
        });
        map.addLayer({
            id:'wms-test-layer',
            type:'raster',
            source:'wms-source'
        });

    })
    return null
}
