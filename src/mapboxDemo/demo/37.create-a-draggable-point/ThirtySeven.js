import React, { useContext } from 'react';
import { Context } from '@/context/MapContext.js';


export default function ThirtySix() {
    const { map, mapboxgl } = useContext(Context);
    const canvas = map.getCanvasContainer();
    let geojson = {
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [0, 0]
                }
            }
        ]
    };
    map.on('load', () => {
        map.addSource('point', {
            type: 'geojson',
            data: geojson
        });
        map.addLayer({
            id: 'point-layer',
            type: 'circle',
            source: 'point',
            paint: {
                'circle-radius': 10,
                'circle-color': '#3887be'
            }
        });

    });
    map.on('mouseenter', 'point-layer', () => {
        map.setPaintProperty('point-layer', 'circle-color', '#3bb2d0');
        canvas.style.cursor = 'move';
    })
    map.on('mouseleave', 'point-layer', function () {
        map.setPaintProperty('point-layer', 'circle-color', '#3887be');
        canvas.style.cursor = '';
    });
    map.on('mousedown','point-layer',(e)=>{
        //防止地图发生拖动
        e.preventDefault()
        map.on('mousemove',onMove);
        map.on('mouseup',onUp);
    })
    const onMove = (e)=>{
        canvas.style.cursor = 'grabbing';
        let {lng,lat} = e.lngLat;
        geojson.features[0].geometry.coordinates = [lng,lat];
        map.getSource('point').setData(geojson);
        canvas.style.cursor = '';
    }
    const onUp = (e)=>{
        map.off('mousemove',onMove);
    }
    return null
}
