import React, { useContext,useState,useEffect,useLayoutEffect } from 'react';
import { Context } from '@/context/MapContext.js';
import "./thirtyeight.scss"

export default function ThirtySix() {
    const { map, mapboxgl } = useContext(Context);
    const canvas = map.getCanvasContainer();
    const [value,setValue] = useState('');
    let places = {
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'theatre'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.038659, 38.931567]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'theatre'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.003168, 38.894651]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'bar'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.090372, 38.881189]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'bicycle'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.052477, 38.943951]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'music'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.031706, 38.914581]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'music'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.020945, 38.878241]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'music'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.007481, 38.876516]
                }
            }
        ]
    };
    useEffect(() => {
        map.on('load', () => {
            map.addSource('places', {
                type: 'geojson',
                data: places
            });
            map.addLayer({
                id: 'point-layer',
                type: 'symbol',
                source: 'places',
                layout: {
                    'icon-image': ['concat', ['get', 'icon'], '-15'],
                    'text-field': ['get', 'icon'],
                    'text-offset': [0, 1.5],
                    'text-font': [
                        'Open Sans Bold',
                        'Arial Unicode MS Bold'
                    ],
                    'text-size': 11,
                    'text-transform': 'uppercase',
                    'text-letter-spacing': 0.05,
                    // 'icon-allow-overlap': true,
                },
                paint: {
                    'text-color':'#202',
                    'text-halo-color':'#fff',
                    'text-halo-width':2
                }
            });
        });
        return () => {
            
        }
    }, [])
    useEffect(() => {
        map.getLayer('point-layer') && map.setFilter('point-layer',['has',value,['get','icon']])
        return () => {
            
        }
    }, [value])
    const inputChange = (e)=>{
        let inputValue = e.target.value;
        setValue(inputValue);
    }
    return (
        <div id="input">
            <input type="text" placeholder="filter by name" onChange={inputChange}></input>
        </div>
    )
}
