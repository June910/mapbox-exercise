import React, { useContext,useState,useEffect } from 'react';
import { Context } from '@/context/MapContext.js';
import "./thirtynine.scss"

export default function ThirtySix() {
    const { map, mapboxgl } = useContext(Context);
    const canvas = map.getCanvasContainer();
    let [featureList,setfeatureList] = useState([]);
    const popup = new mapboxgl.Popup({
        closeButton:false
    });
    let filterFeatures = [];
    const aMouseOver = (feature)=>{
        showPopup(feature);
    }
    const showPopup = (feature)=>{
        let coor = feature.geometry.coordinates;
        let popupContent = `${feature.properties.name}(${feature.properties.iata_code})`;
        popup.setLngLat(coor).setHTML(popupContent).addTo(map);
    }
    const handleChange = (e)=>{
        console.log(featureList);
        let keyWord = e.target.value;
        // setSearchInfo(keyWord);
        map.setFilter('airports-layer',['in',keyWord,['get','abbrev']]);
        let filterFeatures = featureList.filter(feature => {
            return feature.properties.abbrev.indexOf(keyWord) !== -1
        });
        console.log(filterFeatures);
        setfeatureList(filterFeatures);
    }

    map.on('load',()=>{
        map.addSource('airports',{
            type:'vector',
            url:'mapbox://mapbox.04w69w5j'
        });
        map.addLayer({
            id:'airports-layer',
            type:'symbol',
            source:'airports',
            'source-layer':'ne_10m_airports',
            layout:{
                'icon-image':'airport-15',
                'icon-allow-overlap':true
            }
        });
        map.on('mousemove','airports-layer',(e)=>{
            let feature = e.features[0];
            showPopup(feature);
            canvas.style.cursor = "pointer";
        });
        map.on('mouseleave','airports-layer',()=>{
            canvas.style.cursor = '';
            popup.remove();
        });
        map.on('moveend',()=>{
            let features = map.queryRenderedFeatures({
                layers:['airports-layer']
            });
            if(features.length>0){
                // featureList = features.map((feature)=>{ 
                //     return `${feature.properties.name}(${feature.properties.abbrev})`
                // });
                setfeatureList(features);
            }
        })

    })

    return (
        <div className = "map-overlay">
            <fieldset>
                <input id="feature-filter" type="text" placeholder="Filter results by name" onChange = {handleChange}></input>
            </fieldset>
            <div id="feature-listing" className="listing">
                    {
                        featureList.map((feature)=>{
                            let popupContent = `${feature.properties.name}(${feature.properties.abbrev})`;
                            let key = feature.properties.abbrev
                           return <a onMouseOver={e=>aMouseOver(feature)} onMouseLeave = {()=>{popup.remove()}} target='_blank' key={key}>{popupContent}</a>
                        })
                    }
            </div>
        </div>
    )
}
