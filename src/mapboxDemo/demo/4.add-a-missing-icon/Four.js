import React,{useContext} from 'react'
import {Context} from '@/context/MapContext.js'
export default function Four() {
    const {map,mapboxgl} = useContext(Context);
    map.on('styleimagemissing',function(e){
        // let gradientIcon = new GradintIcon(64);
        const id = e.id;
        const rgb = id.split(',').map(Number);
        let gradientIcon = new GradintIcon(64,rgb);

        map.addImage(id,gradientIcon);
        console.log(e);
    })
    map.on('load',function(){
        console.log('mapLoad');
        // map.addImage('gradient-icon',gradientIcon);
        map.addSource('point',{
            type:'geojson',
            data:{
                type:'FeatureCollection',
                features:[
                    {
                        type:'feature',
                        geometry:{
                            type:'Point',
                            coordinates:[117.25,39.06]
                        },
                        properties:{
                            color:'255,129,36'
                        }
                    }
                ]
            }
        });
        map.addLayer({
            id:'gradientlayer',
            type:'symbol',
            source:'point',
            layout:{
                'icon-image':['concat','rgb-',['get','color']]
            }

        })
    })
    return null
}

class GradintIcon{
    constructor(size,rgb){
        this.bytePerPixel = 4
        this.size = size;
        this.width = size;
        this.height = size;
        this.data = new Uint8Array(this.size*this.size*4);
        this.rgb = rgb
        this.GeneratePixel();
    }
    GeneratePixel(){
        for(let i = 0;i<this.size;i++){
            for(let j=0;j<this.size;j++){
               let offset =  (j+i*this.size)*this.bytePerPixel;
               this.data[offset+0] = this.rgb[0];//red
               this.data[offset+1] = this.rgb[1];//green
               this.data[offset+2] = this.rgb[2];//blue
               this.data[offset+3] = 255;
            }
        }
    }
}