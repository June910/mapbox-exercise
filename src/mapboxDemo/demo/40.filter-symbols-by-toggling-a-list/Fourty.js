import React, { useContext, useState, useEffect } from 'react';
import { Context } from '@/context/MapContext.js';
import './fourty.scss';
export default function ThirtySix() {
    const { map, mapboxgl } = useContext(Context);
    let [iconList, setIconList] = useState([]);
    let iconArr = []
    let places = {
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'theatre'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.038659, 38.931567]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'theatre'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.003168, 38.894651]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'bar'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.090372, 38.881189]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'bicycle'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.052477, 38.943951]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'music'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.031706, 38.914581]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'music'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.020945, 38.878241]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'icon': 'music'
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [-77.007481, 38.876516]
                }
            }
        ]
    };

    map.on('load',()=>{
        map.addSource('places',{
            type:'geojson',
            data:places
        });

        places.features.forEach(place => {
            let icon = place.properties.icon
            if(!map.getLayer(icon)){
                map.addLayer({
                    id:icon,
                    type:'symbol',
                    source:'places',
                    layout:{
                        'icon-image':['concat',['get','icon'],'-15'],
                        'icon-allow-overlap':false
                    },
                    filter:['==','icon',icon]
                });
                iconArr.push(icon)
            }

        });
        setIconList(iconArr);
    });
    const handleChange = (e)=>{
        console.log(e.target.checked);
        let icon = e.target.value;
        map.setLayoutProperty(icon,'visibility',e.target.checked? 'visible': 'none');
    }

    return (<div className = "layerControl">
        <ul>
            {iconList.map(icon => <li key={icon}><input onChange={handleChange} type='checkbox' name="layer"  value={icon}/>{icon}</li>)}
        </ul>
        <input  type='checkbox'  />
    </div>)


}
