import React, { useContext, useState, useEffect } from 'react';
import { Context } from '@/context/MapContext.js';

export default function FourtyOne() {
    const { map, mapboxgl } = useContext(Context);
    map.on('load',()=>{
        map.addSource('boundary',{
            type:'geojson',
            data:'https://docs.mapbox.com/mapbox-gl-js/assets/us_states.geojson'
        });
        map.addLayer({
            id:'boundary-fill',
            type:'fill',
            source:'boundary',
            layout:{

            },
            paint:{
                'fill-color':'#627BC1',
                'fill-opacity':[
                    'case',
                    ['boolean',['feature-state','hover'], false],
                    1,0.5
                ]
            }
        });
        map.addLayer({
            id:'boundary-line',
            type:'line',
            source:'boundary',
            paint:{
                'line-color':'#627BC1',
                'line-width':2
            }
        });
        let hoverFeatureId;
        map.on('mousemove','boundary-fill',(e)=>{
            if(e.features.length>0){
                if(hoverFeatureId){
                    map.setFeatureState({
                        source:'boundary',id:hoverFeatureId
                    },{hover:false})
                }
                hoverFeatureId = e.features[0].id;
                map.setFeatureState({
                    source:'boundary',id:hoverFeatureId
                },{hover:true})

            }
        });
        map.on('mouseleave','boundary-fill',(e)=>{
            if(hoverFeatureId){
                map.setFeatureState({
                    source:'boundary',id:hoverFeatureId
                },{hover:false})
            }
            hoverFeatureId = null
        })
    })
    return null
}

