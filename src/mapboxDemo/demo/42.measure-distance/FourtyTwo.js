import React, { useContext, useState, useEffect } from 'react';
import { Context } from '@/context/MapContext.js';
import * as turf from '@turf/turf';
import './fourtytwo.scss';
export default function FourtyTwo() {
    const { map, mapboxgl } = useContext(Context);
    const [length, setLength] = useState(0);
    let geojson = {
        'type': 'FeatureCollection',
        'features': []
    };
    let linestring = {
        'type': 'Feature',
        'geometry': {
            'type': 'LineString',
            'coordinates': []
        }
    };
    map.on('load', () => {
        map.addSource('geojson', {
            type: 'geojson',
            data: geojson
        });
        map.addLayer({
            id:'measure-points',
            type:'circle',
            source:'geojson',
            paint:{
                'circle-radius':5,
                'circle-color':'#000'
            },
            filter:['==','$type','Point']
        });
        map.addLayer({
            id:'measure-lines',
            type:'line',
            source:'geojson',
            layout:{
                'line-cap':'round',
                'line-join':'round'
            },
            paint:{
                'line-color':'#000',
                'line-width':2.5
            },
            filter:['==','$type','LineString']
        });
        map.on('click',(e)=>{
            if(geojson.features.length>1){
                //此时线肯定在geojson中
                //清除线
                geojson.features.pop();
            }
            let {lng,lat} = e.lngLat;
            let point = {
                'type':'Feature',
                'geometry': {
                    'type':'Point',
                    'coordinates':[lng,lat]
                },
                'properties':{
                    'id':String(new Date().getTime())
                }
            };
            linestring.geometry.coordinates.push([lng,lat]);
            geojson.features.push(point);
            geojson.features.push(linestring);
            map.getSource('geojson').setData(geojson);
            setLength(turf.length(linestring));

        })
    })

    return (
        <div className="distance-container">
            <span>
                Total distance:{length}km
            </span>

        </div>
    )
}

