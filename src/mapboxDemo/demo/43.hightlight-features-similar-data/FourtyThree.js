import React, { useContext, useState, useEffect } from 'react';
import { Context } from '@/context/MapContext.js';
import * as turf from '@turf/turf';
import './fourtythree.scss';
export default function FourtyThree() {
    const { map, mapboxgl } = useContext(Context);
    let hoverFeatureId;
    map.on('load', () => {
        map.addSource('counties-source', {
            type: 'vector',
            url: 'mapbox://mapbox.82pkq93d'
        });
        map.addLayer(
            {
                'id': 'counties',
                'type': 'fill',
                'source': 'counties-source',
                'source-layer': 'original',
                'paint': {
                    'fill-outline-color': 'rgba(0,0,0,0.1)',
                    'fill-color': 'rgba(0,0,0,0.1)'
                }
            },
            'settlement-label'
        ); // Place polygon under these labels.
        map.addLayer({
            id: 'countries-highlighted',
            type: 'fill',
            source: 'counties-source',
            'source-layer': 'original',
            paint: {
                'fill-outline-color': 'rgba(0,0,0,0.1)',
                'fill-color': 'rgba(0,0,0,0.5)'
            },
            filter: ['==', ['get', 'COUNTRY'], '']
        })
        map.on('mousemove', 'counties', (e) => {
            let feature = e.features[0];
            let similarAttr = feature.properties.COUNTY;
            if (hoverFeatureId !== feature.id) {
                hoverFeatureId = feature.id;
                map.setFilter('countries-highlighted', ['==', 'COUNTY', similarAttr]);
            }

        })
        map.on('mouseleave','counties',(e)=>{
            console.log(11);
            hoverFeatureId = null;
            map.setFilter('countries-highlighted', ['==', 'COUNTY', '']);

        })
    })

    return (
        <div className="distance-container">
            <span>
                Total population:
            </span>

        </div>
    )
}

