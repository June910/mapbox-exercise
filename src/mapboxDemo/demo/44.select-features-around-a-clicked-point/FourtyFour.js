import React, { useContext, useState, useEffect } from 'react';
import { Context } from '@/context/MapContext.js';
export default function FourtyFour() {
    const { map, mapboxgl } = useContext(Context);
    map.on('load', () => {
        map.addSource('countries', {
            type: 'vector',
            url: 'mapbox://mapbox.82pkq93d'
        });
        map.addLayer({
            id: 'countries-layer',
            type: 'fill',
            source: 'countries',
            'source-layer': 'original',
            paint: {
                'fill-outline-color': 'rgba(0,0,0,0.1)',
                'fill-color': 'rgba(0,0,0,0.1)'
            }
        });
        map.addLayer({
            id: 'countries-highlighted-layer',
            type: 'fill',
            source: 'countries',
            'source-layer': 'original',
            paint: {
                'fill-outline-color': '#484896',
                'fill-color': '#6e599f',
                'fill-opacity': 0.75
            },
            filter: ['in', 'FIPS', '']
        },
            'settlement-label'
        )
    });
    map.on('click','countries-layer',(e)=>{
        let {x, y} = e.point;
        console.log(x, y);
         let bbox = [[x - 5, y - 5], [x + 5, y + 5]];
         let features = map.queryRenderedFeatures(bbox,{
             layers:['countries-layer']
         });
         let idList = features.map(feature => feature.properties.FIPS);
         map.setFilter('countries-highlighted-layer',['in',['get','FIPS'],['literal',idList]]);

    })


    return null
}

