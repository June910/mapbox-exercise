import React, { useContext, useState, useEffect } from 'react';
import { Context } from '@/context/MapContext.js';
export default function FourtyFour() {
    const { map, mapboxgl } = useContext(Context);
    let bounds = [
        [-74.04728500751165, 40.68392799015035], // Southwest coordinates
        [-73.91058699000139, 40.87764500765852] // Northeast coordinates
    ]

    // var map = new mapboxgl.Map({
    //     container: 'map',
    //     style: 'mapbox://styles/mapbox/streets-v11',
    //     center: [-73.9978, 40.7209],
    //     zoom: 13,
    //     maxBounds: bounds // Sets bounds as max
    //     });
    map.on('load',()=>{
        map.setMaxBounds(bounds);
    })



    return null
}

