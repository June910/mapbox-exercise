import React, { useContext, useState, useEffect } from 'react';
import { Context } from '@/context/MapContext.js';
import * as d3 from 'd3';
import axios from 'axios';
import './fourtysix.scss';
export default function FourtySix() {
    const { map, mapboxgl } = useContext(Context);
    let url = 'https://docs.mapbox.com/mapbox-gl-js/assets/significant-earthquakes-2015.geojson';
    let months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];
    map.on('load', () => {
        // d3.json(url,(err,data)=>{
        //     if(err) throw err;
        //     data.features = data.features.map((feature)=>{
        //         feature.properties.month = new Date(feature.properties.time).getMonth();
        //         return feature
        //     });
        //     map.addSource('earthquakes',{
        //         type:'geojson',
        //         data:data
        //     });
        //     map.addLayer({
        //         id:'earthquake-circles',
        //         type:'circle',
        //         source:'earthquakes',
        //         paint:{
        //             'circle-color':[
        //                 'interpolate',
        //                 ['get','mag'],
        //                 6,'#FCA107',8,'7F3121'
        //             ],
        //             'circle-opacity':0.75,
        //             'circle-radius':[
        //                 'interpolate',
        //                 ['linear'],
        //                 ['get','mag'],
        //                 6,20,8,40
        //             ]
        //         }
        //     });
        //     map.addLayer({
        //         id:'earthquake-labels',
        //         type:'symbol',
        //         source:'earthquakes',
        //         layout:{
        //             'text-field':['concat',['to-string',['get','mag']],'m'],
        //             'text-font':['Open Sans Bold','Arial Unicode MS Bold'],
        //             'text-size':12
        //         },
        //         paint:{
        //             'text-color':'rgba(0,0,0,0.5)'
        //         }
        //     });

        // })
        axios.get(url).then(data => {
            console.log(data);
            let geojson = data.data;
            geojson.features = geojson.features.map((feature) => {
                feature.properties.month = new Date(feature.properties.time).getMonth();
                return feature
            });
            console.log(geojson);
            map.addSource('earthquakes', {
                type: 'geojson',
                data: geojson
            });
            map.addLayer({
                id: 'earthquake-circles',
                type: 'circle',
                source: 'earthquakes',
                paint: {
                    'circle-color': [
                        'interpolate',
                        ['linear'],
                        ['get', 'mag'],
                        6, '#FCA107', 8, '#7F3121'
                    ],
                    'circle-opacity': 0.75,
                    'circle-radius': [
                        'interpolate',
                        ['linear'],
                        ['get', 'mag'],
                        6, 20, 8, 40
                    ]
                }
            });
            map.addLayer({
                id: 'earthquake-labels',
                type: 'symbol',
                source: 'earthquakes',
                layout: {
                    'text-field': ['concat', ['to-string', ['get', 'mag']], 'm'],
                    'text-font': ['Open Sans Bold', 'Arial Unicode MS Bold'],
                    'text-size': 12
                },
                paint: {
                    'text-color': 'rgba(0,0,0,0.5)'
                }
            });
            filterBy(0);
        }).catch(e => {
            console.log(e)
        });
    });
    const handleChange = (e) => {
        console.log(e.target.value);
        let month = parseInt(e.target.value)
        filterBy(month);
    }
    const filterBy = (month)=>{
        map.setFilter('earthquake-circles',['==','month',month]);
        map.setFilter('earthquake-labels',['==','month',month]);

    }

    return (
        <div className="map-overlay top">
            <div className="map-overlay-inner">
                <h2>Significant earthquakes in 2015</h2>
                <label id="month"></label>
                <input id="slider" type="range" min="0" max="11" step="1" defaultValue="0" onChange={handleChange} />
            </div>
            <div className="map-overlay-inner">
                <div  className="legend">
                    <div className="bar"></div>
                    <div>Magnitude (m)</div>
                </div>
            </div>
        </div>
    )
}

