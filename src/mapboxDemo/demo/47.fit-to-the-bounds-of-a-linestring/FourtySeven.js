import React, { useContext, useState, useEffect } from 'react';
import { Context } from '@/context/MapContext.js';
import './fourtyseven.scss';
export default function FourtySeven() {
    const { map, mapboxgl } = useContext(Context);
    let geojson = {
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'properties': {},
                    'coordinates': [
                        [-77.0366048812866, 38.89873175227713],
                        [-77.03364372253417, 38.89876515143842],
                        [-77.03364372253417, 38.89549195896866],
                        [-77.02982425689697, 38.89549195896866],
                        [-77.02400922775269, 38.89387200688839],
                        [-77.01519012451172, 38.891416957534204],
                        [-77.01521158218382, 38.892068305429156],
                        [-77.00813055038452, 38.892051604275686],
                        [-77.00832366943358, 38.89143365883688],
                        [-77.00818419456482, 38.89082405874451],
                        [-77.00815200805664, 38.88989712255097]
                    ]
                }
            }
        ]
    };
    map.on('load',()=>{
        map.addSource('line',{
            type:'geojson',
            data:geojson
        });
        map.addLayer({
            id:'line-layer',
            type:'line',
            source:'line',
            layout:{
                'line-join':'round',
                'line-cap':'round'
            },
            paint:{
                'line-color':'#BF93E4',
                'line-width': 5
            }
        })
    });
    const handleZoom = ()=>{
        let coordinates = geojson.features[0].geometry.coordinates;
        let bounds = coordinates.reduce((bounds,coor)=>{
            return bounds.extend(coor)
        },new mapboxgl.LngLatBounds(coordinates[0],coordinates[1]))
        console.log(bounds);
        // let xMin = Math.min(...coordinates.map(coor => coor[0]));
        // let yMin = Math.min(...coordinates.map(coor => coor[1]));
        // let xMax = Math.max(...coordinates.map(coor => coor[0]));
        // let yMax = Math.max(...coordinates.map(coor => coor[1]));
        // let bounds = [xMin,yMin,xMax,yMax];
        // let bounds = coordinates.reduce
        map.fitBounds(bounds,{
            padding:20
        });
    }
    return (
            <button id="zoomto" className="btn-control" onClick={handleZoom}>Zoom to bounds</button>
    )
}

