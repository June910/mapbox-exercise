import React, { useContext, useState, useEffect } from 'react';
import { Context } from '@/context/MapContext.js';
export default function FourtySeven() {
    const { map, mapboxgl } = useContext(Context);
    map.boxZoom.disable();
    let canvas = map.getCanvas();
    let startPoint;
    let rectGeojson = {
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'Polygon',
                    'properties': {},
                    'coordinates': [
                    ]
                }
            }
        ]
    };

    const onMouseMove = (e)=>{
        
        // e.preventDefault();
        let endPoint = e.lngLat;
        let beginPoint = [startPoint.lng,startPoint.lat];
        let secondPoint = [endPoint.lng,startPoint.lat];
        let thirdPoint = [endPoint.lng,endPoint.lat];
        let fourthPoint = [startPoint.lng,endPoint.lat];
        rectGeojson.features[0].geometry.coordinates.push(secondPoint,thirdPoint,fourthPoint,beginPoint)
        map.getSource('box').setData(rectGeojson);

        console.log(e);
    }

    const onMouseUp = ()=>{
        map.off('mousemove',onMouseMove);
    }



    const onMouseDown = (e)=>{
        if(!e.shiftKey) return
        map.dragPan.disable();
        startPoint = e.lngLat;

        rectGeojson.features[0].geometry.coordinates.push([startPoint.lng,startPoint.lat])
        map.on('mousemove',onMouseMove);
        map.on('mouseup',onMouseUp);
    }



    map.on('load', () => {
        map.addSource('box',{
            type:'geojson',
            data:rectGeojson
        });
        map.addLayer({
            id:'rectBox',
            type:'fill',
            source:'box',
            paint:{
                'fill-color':'rgba(135,206,235,0.2)',
                'fill-outline-color':'rgb(0,0,139)'
            }
        })
        map.addSource('countries', {
            'type': 'vector',
            'url': 'mapbox://mapbox.82pkq93d'
        });
        map.addLayer({
            id:'countries',
            type:'fill',
            source:'countries',
            'source-layer':'original',
            'paint':{
                'fill-outline-color':'rgba(0,0,0,0.1)',
                'fill-color':'rgba(0,0,0,0.1)'
            }
        },
        'settlement-label'
        );
        map.addLayer({
            id:'countries-hightlight',
            type:'fill',
            source:'countries',
            'source-layer':'original',
            'paint':{
                'fill-outline-color': '#484896',
                'fill-color': '#6e599f',
                'fill-opacity': 0.75
            },
            filter:['in','FIPS','']
        },'settlement-label');
        canvas.addEventListener('mousedown',onMouseDown,true);
        // canvas.addEventListener('mouseup',onMouseUp,true);


        // canvas.addEventListener('keydown',(e)=>{
        //     if(!e.shiftKey) return
        //     map.on('mousedown','countries',onEnter)
        // })
        // (e)=>{
        //     // if(e.button !== 0){
        //     //     //鼠标左键
        //     //     return
        //     // }
        //     let {x:x0,y:y0} = e.point;
        //     console.log(x0,y0)
            
        //     map.on('mouseup',searchFeatures);
        //     map.off('mouseenter');
        //     map.off('mouseup',onUp);
            
        // }
        // const isShift = (e)=>{
        //     return e.shiftKey
        // }
        // const onUp = (e,startPoint)=>{
        //     let x0 = startPoint[0];
        //     let y0 = startPoint[1];
        //     let {x:x1,y:y1} = e.point;
        //     console.log(x1,y1)
        //     let bounds = [[x0,y0],[x1,y1]];
        //     let features = map.queryRenderedFeatures(bounds,{
        //         layers:['countries']
        //     }); 
        //     map.off('mouseenter',onEnter);
        //     map.off('mouseup',onUp);
        // }
        // const onEnter = (e)=>{
        //     let {x:x0,y:y0} = e.point;
        //     map.on('mouseup',onUp)


        // }
    });



    return null


}

