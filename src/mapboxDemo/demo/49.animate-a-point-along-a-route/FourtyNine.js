import React, { useContext, useState, useEffect } from 'react';
import { Context } from '@/context/MapContext.js';
import * as turf from '@turf/turf';



export default function FourtyNine() {
    const { map, mapboxgl } = useContext(Context);


    let origin = [-122.414, 37.776];

    // Washington DC
    let destination = [-77.032, 38.913];

    // A simple line from origin to destination.
    let route = {
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [origin, destination]
                }
            }
        ]
    };
    let point = {
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'Point',
                    'coordinates': origin
                }
            }
        ]
    };

    let lineDistance = turf.distance(turf.point(origin),turf.point(destination), {
        units:'kilometers'
        });

    let arc = [];
    const steps = 500;
    for (let i = 0; i < lineDistance; i += lineDistance / steps) {
        let segment = turf.along(route.features[0], i, {
            units:'kilometers'
        });
        arc.push(segment.geometry.coordinates);
    }
    route.features[0].geometry.coordinates = arc;

    map.on('load', () => {
        map.addSource('route', {
            type: 'geojson',
            data: route
        });

        map.addSource('point', {
            type: 'geojson',
            data: point
        })
        map.addLayer({
            'id': 'route',
            'source': 'route',
            'type': 'line',
            'paint': {
                'line-width': 2,
                'line-color': '#007cbf'
            }
        });

        map.addLayer({
            'id': 'point',
            'source': 'point',
            'type': 'symbol',
            'layout': {
                'icon-image': 'airport-15',
                'icon-rotate': ['get', 'bearing'],
                'icon-rotation-alignment': 'map',
                'icon-allow-overlap': true,
                'icon-ignore-placement': true
            }
        });
        fly();

    })
    let index = 0

    const fly = ()=>{
        if(index<steps-1){
            console.log(index);
            point.features[0].geometry.coordinates = route.features[0].geometry.coordinates[index];
            point.features[0].properties.bearing = turf.bearing(route.features[0].geometry.coordinates[index],route.features[0].geometry.coordinates[index+1]);
            map.getSource('point').setData(point);
            index++;
        }
        else{
            index = 0;
        }

        requestAnimationFrame(fly);

    }




    return null


}

