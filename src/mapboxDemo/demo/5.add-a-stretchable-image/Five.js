import React,{useContext} from 'react'
import {Context} from '@/context/MapContext'
export default function Five() {
    const {map,mapboxgl} = useContext(Context);
    var imageUrls = {
        'popup': 'https://docs.mapbox.com/mapbox-gl-js/assets/popup.png',
        'popup-debug':'https://docs.mapbox.com/mapbox-gl-js/assets/popup_debug.png'
        };
    loadImages(imageUrls,map);
    map.on('load',function(){
        console.log('five map load');
        map.addSource('points',{
            type:'geojson',
            data:{
                type:'FeatureCollection',
                features:[
                    {
                        type:'Feature',
                        geometry:{
                            type:'Point',
                            coordinates:[117.69,39.06]
                        },
                        properties:{
                            'image-name':'popup-debug',
                            'name':'Line1\nLine2\nLine3'
                        }
                    },
                    {
                        type:'Feature',
                        geometry:{
                            type:'Point',
                            coordinates:[117.58,39.08]
                        },
                        properties:{
                            'image-name':'popup-debug',
                            'name':'Two longer line'
                        }
                    }
                ]
            }
        });
        map.addLayer({
            id:'popup',
            type:'symbol',
            source:'points',
            layout:{
                'text-field':['get','name'],
                'icon-image':['get','image-name'],
                'icon-text-fit':'both',
                'icon-allow-overlap':true,
                'text-allow-overlap':true
            }
        })
    })
    return null
}


function loadImages(urls,map){
    for(let name in urls){
        map.loadImage(urls[name],function(err,image){
            if(err){

            }else{
                map.addImage(name,image,{
                    stretchX:[
                        [25,55],
                        [85,115]
                    ],
                    stretchY:[[25,100]],
                    content:[25,25,115,100],
                    pixelRatio:2
                });
            }
        })
    }
}
