import React, { Component } from 'react';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { MapControls } from 'three/examples/jsm/controls/OrbitControls';
import { BufferGeometryUtils } from 'three/examples/jsm/utils/BufferGeometryUtils';
import Stats from 'three/examples/jsm/libs/stats.module';
import {OBJLoader} from 'three/examples/jsm/loaders/OBJLoader';
import {MTLLoader} from 'three/examples/jsm/loaders/MTLLoader';
import {EffectComposer} from 'three/examples/jsm/postprocessing/EffectComposer';
import {RenderPass} from 'three/examples/jsm/postprocessing/RenderPass';
import {ShaderPass} from 'three/examples/jsm/postprocessing/ShaderPass';
import {UnrealBloomPass} from 'three/examples/jsm/postprocessing/UnrealBloomPass';
import {CopyShader} from 'three/examples/jsm/shaders/CopyShader';
import {LUminosityHighPassShader} from 'three/examples/jsm/shaders/LuminosityHighPassShader';
export default class SmartCity extends Component {
    constructor() {
        super();
        this.handleMouseDown = this.handleMouseDown.bind(this);
        this.handleResize = this.handleResize.bind(this);
    }
    componentDidMount(){
        this.init();
        this.loadOBJ('./models/obj/city_test/city.obj', './models/obj/city_test/city.mtl');
        window.addEventListener('click',this.handleMouseDown);
        window.addEventListener('resize',this.handleResize);
    }
    init() {
        const width = window.innerWidth;
        const height = window.innerHeight;
        const s = width / height;
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.TextureLoader().load("./texture/sky.jpg");
        this.camera = new THREE.PerspectiveCamera(30, s, 1, 100);
        this.camera.position.set(8, 4, 5);
        this.scene.add(this.camera);
        let boxGeometry = new THREE.BoxGeometry(2,2,2);
        let boxMaterial = new THREE.MeshLambertMaterial({
            color:0xFF0000
        });
        let boxMesh = new THREE.Mesh(boxGeometry,boxMaterial);
        // this.scene.add(boxMesh);
        // initLight
        let light0 = new THREE.AmbientLight(0xffffff, 0.2);
        let light1 = new THREE.PointLight(0xfafafa, 0.8);
        light1.position.set(200, 90, 40);
        // let light2 = new THREE.PointLight(0xfafafa, 0.4);
        // light2.position.set(200, 90, -40);
        this.scene.add(light0,light1);

        //addGridHelper
        const gh = new THREE.GridHelper(60, 160, new THREE.Color(0x55555), new THREE.Color(0x333333));
        this.scene.add(gh);

        //add axesHelper
        let axesHelper = new THREE.AxesHelper(250);
        this.scene.add(axesHelper);

        //add status
        this.stats = new Stats();
        document.getElementById('space').appendChild(this.stats.domElement);

        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(width, height);
        this.renderer.setClearColor(0xb9d3ff, 1);

        //addControl
        const controls = new MapControls(this.camera, this.renderer.domElement);
        controls.enableDamping = true;
        controls.dampingFactor = 0.25;
        controls.screenSpacePanning = false;
        controls.maxDistance = 5000;
        controls.addEventListener('change', () => {
            this.renderer.render(this.scene, this.camera);
        });
        document.getElementById('space').appendChild(this.renderer.domElement);
        this.animate();
    }
    loadOBJ(objUrl, mtlUrl){
        const objLoader = new OBJLoader();
        const mtlLoader = new MTLLoader();
        mtlLoader.load(mtlUrl,(materials)=>{
            materials.preload();
            console.log(materials);
            objLoader.setMaterials(materials);
            objLoader.load(objUrl,(obj)=>{
                this.scene.add(obj);
                // obj.position.set(0,0,0);
                obj.translateX(-20);
                obj.translateZ(-30);

                console.log(obj);
                const cubeTexture = new THREE.CubeTextureLoader().setPath('./texture/').load(new Array(6).fill('start.jpg'));
                console.log(cubeTexture);
                obj.children.forEach((mesh)=>{
                    mesh.material.envMap  = cubeTexture;
                })

            })
        })
    }

    animate() {
        requestAnimationFrame(()=>{
            this.animate();
        });
        this.renderer.render(this.scene, this.camera);
        this.stats.update();
    }
    handleMouseDown(evt){
        console.log(this.scene);
        const rayCaster = new THREE.Raycaster();
        let {clientX, clientY} = evt;
        let mouse = new THREE.Vector2();
        mouse.x = (clientX / window.innerWidth)*2 - 1;
        mouse.y = -(clientY / window.innerHeight) * 2 + 1;
        rayCaster.setFromCamera(mouse, this.camera);
        // console.log(this.scene.children.map(item => item.type === 'Group'));
        console.log(this.scene.children[5].children);
        const intersect = rayCaster.intersectObjects(this.scene.children[5].children,false);
        // const intersect = rayCaster.intersectObjects(this.scene.children);

        console.log(intersect);

    }
    handleResize(){
        const width = window.innerWidth;
        const height = window.innerHeight;
        this.renderer.setSize(width, height);
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
    }
    render() {
        return (
            <div id="space">
            </div>
        )
    }
}

