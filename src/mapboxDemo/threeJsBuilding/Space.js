import React, { Component } from 'react';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { MapControls } from 'three/examples/jsm/controls/OrbitControls';
import { BufferGeometryUtils } from 'three/examples/jsm/utils/BufferGeometryUtils';
import Stats from 'three/examples/jsm/libs/stats.module';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import * as geolib from 'geolib';
import './space.scss';
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader';


export default class Space extends Component {
    constructor() {
        super();
        this.buildings = [];
        this.helperBuilding = [];
        this.buildingMaterial = new THREE.MeshLambertMaterial({
            color: 0xF5F5F5
        });
        this.center = [117.649, 39.019];
        this.scanCenter = [117.686007, 39.02];



        const vertexShader = `
        #define PHONG

        varying vec3 vViewPosition;
        
        #ifndef FLAT_SHADED
        
            varying vec3 vNormal;
            varying vec2 Vposition;
        
        #endif
        
        #include <common>
        #include <uv_pars_vertex>
        #include <uv2_pars_vertex>
        #include <displacementmap_pars_vertex>
        #include <envmap_pars_vertex>
        #include <color_pars_vertex>
        #include <fog_pars_vertex>
        #include <morphtarget_pars_vertex>
        #include <skinning_pars_vertex>
        #include <shadowmap_pars_vertex>
        #include <logdepthbuf_pars_vertex>
        #include <clipping_planes_pars_vertex>
        
        void main() {
        
            #include <uv_vertex>
            #include <uv2_vertex>
            #include <color_vertex>
        
            #include <beginnormal_vertex>
            #include <morphnormal_vertex>
            #include <skinbase_vertex>
            #include <skinnormal_vertex>
            #include <defaultnormal_vertex>
        
        #ifndef FLAT_SHADED // Normal computed with derivatives when FLAT_SHADED
        
            vNormal = normalize( transformedNormal );
        
        #endif
        
            #include <begin_vertex>
            #include <morphtarget_vertex>
            #include <skinning_vertex>
            #include <displacementmap_vertex>
            #include <project_vertex>
            #include <logdepthbuf_vertex>
            #include <clipping_planes_vertex>
        
            vViewPosition = - mvPosition.xyz;
            Vposition = vec2(position.x, position.z);
        
            #include <worldpos_vertex>
            #include <envmap_vertex>
            #include <shadowmap_vertex>
            #include <fog_vertex>
        
        }
        
        `;

        const fragmentShader = `
        #define PHONG

        uniform vec3 diffuse;
        uniform vec3 emissive;
        uniform vec3 specular;
        uniform float shininess;
        uniform float opacity;
        varying vec2 Vposition;
        uniform vec3 Ucolor;
        uniform float UInradius;
        uniform float UOutradius;
        uniform vec2 Ucenter;
        
        #include <common>
        #include <packing>
        #include <dithering_pars_fragment>
        #include <color_pars_fragment>
        #include <uv_pars_fragment>
        #include <uv2_pars_fragment>
        #include <map_pars_fragment>
        #include <alphamap_pars_fragment>
        #include <aomap_pars_fragment>
        #include <lightmap_pars_fragment>
        #include <emissivemap_pars_fragment>
        #include <envmap_common_pars_fragment>
        #include <envmap_pars_fragment>
        #include <cube_uv_reflection_fragment>
        #include <fog_pars_fragment>
        #include <bsdfs>
        #include <lights_pars_begin>
        #include <lights_phong_pars_fragment>
        #include <shadowmap_pars_fragment>
        #include <bumpmap_pars_fragment>
        #include <normalmap_pars_fragment>
        #include <specularmap_pars_fragment>
        #include <logdepthbuf_pars_fragment>
        #include <clipping_planes_pars_fragment>
        
        void main() {
        
            #include <clipping_planes_fragment>
        
            vec4 diffuseColor = vec4( diffuse, opacity );
            ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
            vec3 totalEmissiveRadiance = emissive;
        
            #include <logdepthbuf_fragment>
            #include <map_fragment>
            #include <color_fragment>
            #include <alphamap_fragment>
            #include <alphatest_fragment>
            #include <specularmap_fragment>
            #include <normal_fragment_begin>
            #include <normal_fragment_maps>
            #include <emissivemap_fragment>
        
            // accumulation
            #include <lights_phong_fragment>
            #include <lights_fragment_begin>
            #include <lights_fragment_maps>
            #include <lights_fragment_end>
        
            // modulation
            #include <aomap_fragment>
        
            vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;

        
            #include <envmap_fragment>
            float d = distance(Ucenter,Vposition);
            if(d > UInradius/100.0 && d<= UOutradius/100.0){
                gl_FragColor = vec4(Ucolor * outgoingLight, diffuseColor.a);
            }else{
                gl_FragColor = vec4( outgoingLight, diffuseColor.a );
            }
        
            #include <tonemapping_fragment>
            #include <encodings_fragment>
            #include <fog_fragment>
            #include <premultiplied_alpha_fragment>
            #include <dithering_fragment>
        
        }
        `;

        const vertexShader1 = `
        varying vec3 Vposition;
        varying vec2 vUv;
        varying 
        void main(){
            vUv = uv;
            Vposition = position;
            gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
        }
        `;
        const fragmentShader1 = `
        varying vec3 Vposition;
        uniform vec3 Ucolor;
        uniform float Uradius;
        uniform vec2 Ucenter;
        varying vec2 vUv;
        uniform sampler2D tDiffuse;
        float getLength(float x, float y){
            return sqrt(x*x + y*y);
        }
        void main(){
            vec4 color = texture2D(tDiffuse,vUv);
            vec3 luma = vec3( 0.299, 0.587, 0.7);
            float v = dot( color.rgb, luma);
            float uLength = getLength(Vposition.x, Vposition.y);
            if(Vposition.z <= 0.0){
                    gl_FragColor = vec4(color.rgb, 0.2);
                }else{
                    gl_FragColor = vec4(color.r, color.g, 0.0, 0.2);
                }
        }
        `;
        this.customBuildingMaterial = new THREE.ShaderMaterial({
            uniforms: THREE.UniformsUtils.merge([
                THREE.ShaderLib['phong'].uniforms,
                {
                    Ucolor: {
                        value: new THREE.Color(0x00ff00),
                    },
                    UInradius: {
                        value: 10.0
                    },
                    UOutradius: {
                        value: 100.0
                    },
                    Ucenter: {
                        value: this.computedCenter()
                    },
                    tDiffuse: { value: null }
                }
            ])
            ,
            vertexShader: vertexShader,
            fragmentShader: fragmentShader

        });
        this.customBuildingMaterial.lights = true;
        // this.customBuildingMaterial.color = new THREE.Color(0xffff00);
        // this.customBuildingMaterial.uniforms.diffuse.value = new THREE.Color(0xffff00);
        this.scanShader = {
            uniforms: {
                Ucolor: {
                    value: new THREE.Color(0xff0000),
                },
                Uradius: {
                    value: 0.0
                },
                Ucenter: {
                    value: this.computedCenter()
                },
                tDiffuse: { value: null }
            },
            vertexShader: vertexShader1,
            fragmentShader: fragmentShader1
        }
        this.roadMaterial = new THREE.LineBasicMaterial({
            color: 0x808080
        })
        this.fireGeometry = null;
        this.animateLineList = [];

    }
    componentDidMount() {
        this.init();
        document.getElementById('space').addEventListener('click', (evt) => {
            let mouse = {
                x: (evt.clientX / window.innerWidth) * 2 - 1,
                y: -(evt.clientY / window.innerHeight) * 2 + 1
            }

            this.fire(mouse);


        })
    }
    computedCenter() {
        let relativeCenterArr = this.caculateRelativePosition(this.scanCenter, this.center);
        let relativeCenterVec = new THREE.Vector2(relativeCenterArr[0], -relativeCenterArr[1]);
        return relativeCenterVec


    }
    init() {

        const width = window.innerWidth;
        const height = window.innerHeight;
        const s = width / height;

        // initScene
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0x222222);


        // initCamera
        this.camera = new THREE.PerspectiveCamera(25, s, 1, 1000);
        this.camera.position.set(20, 150, 100);
        this.camera.lookAt(80, 0, 0);
        this.scene.add(this.camera);

        //init rayCaster
        this.rayCaster = new THREE.Raycaster();



        // initLight
        let light0 = new THREE.AmbientLight(0xfafafa, 0.25);
        let light1 = new THREE.PointLight(0xfafafa, 0.4);
        light1.position.set(-200, 90, 40);
        let light2 = new THREE.PointLight(0xfafafa, 0.4);
        light2.position.set(-200, 90, -40);
        this.scene.add(light0, light1, light2);



        //addGridHelper
        const gh = new THREE.GridHelper(60, 160, new THREE.Color(0x55555), new THREE.Color(0x333333));
        this.scene.add(gh);

        //add axesHelper
        let axesHelper = new THREE.AxesHelper(250);
        this.scene.add(axesHelper);

        //add status
        this.stats = new Stats();
        document.getElementById('space').appendChild(this.stats.domElement);




        //addMesh
        // this.addMesh();


        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(width, height);
        // this.renderer.setClearColor(0xb9d3ff, 1);







        const renderPass = new RenderPass(this.scene, this.camera);
        const scanShaderPass = new ShaderPass(this.scanShader);
        const effectCopy = new ShaderPass(CopyShader);
        effectCopy.renderToScreen = true
        // scanShaderPass.renderToScreen = true;
        this.composer = new EffectComposer(this.renderer);
        this.composer.addPass(renderPass);
        // this.composer.addPass(scanShaderPass);
        this.composer.addPass(effectCopy);


        //addControl
        const controls = new MapControls(this.camera, this.renderer.domElement);
        controls.enableDamping = true;
        controls.dampingFactor = 0.25;
        controls.screenSpacePanning = false;
        controls.maxDistance = 5000;
        controls.addEventListener('change', () => {
            this.renderer.render(this.scene, this.camera);
        });
        // controls.update();

        document.getElementById('space').appendChild(this.renderer.domElement);
        // this.renderer.render(this.scene, this.camera);
        this.animate();
        window.addEventListener('resize', () => {
            this.resize();
        });


        this.getBuilding();
        this.getRoad();
    }
    resize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
    }

    addMesh() {
        let geometry = new THREE.BoxGeometry(1, 1, 1);
        let material = new THREE.MeshLambertMaterial({
            color: '0x7FFF00'
        });
        let mesh = new THREE.Mesh(geometry, material);
        this.scene.add(mesh);
    }
    animate() {
        requestAnimationFrame(() => {
            this.animate();
        });
        // this.renderer.render(this.scene, this.camera);
        this.composer.render();
        this.stats.update();

        this.updateRadius();
        this.updateAnimateLine();
    }
    getBuilding() {
        // console.log('get builidng');
        fetch('../../../buildings.json').then((res) => {
            res.json().then(data => {
                this.generateBuilding(data);
            })
        })
        // axios.get('../../../buildings.json').then(data=>{
        //     console.log(data);
        // })
    }

    getRoad() {
        fetch('../../../road.json').then((res) => {
            res.json().then(data => {
                this.generateRoad(data);
            })
        })
    }


    generateRoad(data) {
        let features = data.features;
        features.forEach(feature => {
            let coorList = feature.geometry.coordinates;
            let properties = feature.properties;
            let relativeCoorList = coorList.map((coorArr) => {
                return this.caculateRelativePosition(coorArr, this.center)
                // return this.caculateRelativePosition(coorArr);

            });


            this.genRoadGeometry(relativeCoorList, properties);
        });
        // let mergeGeometry = BufferGeometryUtils.mergeBufferGeometries(this.buildings);
        // let mesh = new THREE.Mesh(mergeGeometry,this.material);
        // this.scene.add(mesh);
    }


    generateBuilding(data) {
        let features = data.features;
        features.forEach(feature => {
            let coorList = feature.geometry.coordinates[0];
            let properties = feature.properties;
            let relativeCoorList = coorList.map((coorArr) => {
                return this.caculateRelativePosition(coorArr, this.center)
                // return this.caculateRelativePosition(coorArr);

            });
            this.genGeometry(relativeCoorList, properties);
        });
        let mergeGeometry = BufferGeometryUtils.mergeBufferGeometries(this.buildings);

        let mesh = new THREE.Mesh(mergeGeometry, this.customBuildingMaterial);
        this.scene.add(mesh);

    }
    genRoadGeometry(relativeCoorList, info) {
        let points = [];


        relativeCoorList.forEach(point => {
            let [x, y] = point;
            points.push(new THREE.Vector3(x, y, 0))
        });
        let geometry = new THREE.BufferGeometry().setFromPoints(points);
        let line = new THREE.Line(geometry, this.roadMaterial);

        line.computeLineDistances();
        let distance = geometry.attributes.lineDistance.array[geometry.attributes.lineDistance.count - 1];
        if (distance > 0.8) {
            let aniLine = this.addAnimateLine(geometry, distance);
            this.animateLineList.push(aniLine);
            aniLine.rotateX(-Math.PI / 2);
            // aniLine.rotateY(Math.PI);
            this.scene.add(aniLine);

        }
        line.rotateX(-Math.PI / 2);
        // line.rotateY(Math.PI);
        this.scene.add(line);
    }

    addAnimateLine(geometry, length) {
        let animateLine = new THREE.Line(geometry, new THREE.LineDashedMaterial({
            color: 0x00FFFF
        }));
        animateLine.material.dashSize = 0;
        animateLine.material.gapSize = 10000;
        animateLine.material.transparent = true;
        animateLine.length = length;
        return animateLine
    }
    updateAnimateLine() {
        if (this.animateLineList.legnth <= 0) return
        this.animateLineList.forEach((line) => {
            let dash = line.material.dashSize;
            let length = line.length;
            if (dash > length) {

                //recove
                line.material.dashSize = 0;
                line.material.opacity = 1;
            } else {
                line.material.dashSize += length / 1000;
                line.material.opacity = line.material.opacity > 0 ? line.material.opacity - 1 / length * 0.01 : 0;
            }
        })
    }
    updateRadius() {
        if (this.customBuildingMaterial.uniforms.UInradius.value > 3000) {
            this.customBuildingMaterial.uniforms.UInradius.value = 0.0;
            this.customBuildingMaterial.uniforms.UOutradius.value = 90.0;
        }
        this.customBuildingMaterial.uniforms.UInradius.value += 10;
        this.customBuildingMaterial.uniforms.UOutradius.value += 10;

    }
    genGeometry(relativeCoorList, info) {
        let shape = new THREE.Shape();
        for (let i = 0; i <= relativeCoorList.length - 1; i++) {
            let coorArr = relativeCoorList[i];
            if (i == 0) {
                shape.moveTo(coorArr[0], coorArr[1])
            }
            else {
                shape.lineTo(coorArr[0], coorArr[1])
            }
        }
        let height = info['层数1'] * 0.05;
        let geometry = new THREE.ExtrudeBufferGeometry(shape, {
            curveSegments: 1,
            depth: height,
            bevelEnabled: false
        });
        geometry.rotateX(-Math.PI / 2);
        //    geometry.rotateZ(Math.PI);
        this.buildings.push(geometry);
        let helper = this.genHelper(geometry);
        if (helper) {
            helper.name = '一共有' + info['层数1'];
            this.helperBuilding.push(helper)
        }



    }
    fire(mouse) {
        // object.material.color.set( 0xff0000 )
        this.rayCaster.setFromCamera(mouse, this.camera);
        let intersects = this.rayCaster.intersectObjects(this.helperBuilding, true);
        if (intersects.length > 0) {
            console.log(intersects[0].object);
            this.scene.add(intersects[0].object);
            //     if(this.fireGeometry !== intersects[0].object){
            //         if(this.fireGeometry){
            //         // this.fireGeometry.material.emissive.setHex(this.fireGeometry.currentHex);
            //         this.fireGeometry.material.color = '0xffffff'
            //         }
            //         this.fireGeometry = intersects[0].object;
            //         // this.fireGeometry.currentColor = this.fireGeometry.material.color;
            //         this.fireGeometry.material.color = 'oxffb6c1';
            //         this.scene.add(this.fireGeometry);


            // }else{
            //     // if ( this.fireGeometry ) this.fireGeometry.material.emissive.setHex( this.fireGeometry.currentHex );
            //     this.fireGeometry = null
            // }
        }
    }
    genHelper(geo) {
        if (!geo.boundingBox) geo.computeBoundingBox();
        let box3 = geo.boundingBox;
        // if(!isFinite(box3.max.x)){
        //     return false
        // }
        let helper = new THREE.Box3Helper(box3, 0xffff00);
        if (helper) {
            helper.updateMatrixWorld();
            return helper
        }


    }

    //方法1 通过geo-lib计算经纬度之间的长度，然后算出相对位置
    caculateRelativePosition(start, end) {

        //
        let distance = geolib.getDistance(start, end);
        let bearing = geolib.getRhumbLineBearing(end, start);
        //    let x = start[0] + (distance * Math.cos(bearing * Math.PI / 180));
        //    let y = start[1] + (distance * Math.sin(bearing * Math.PI / 180));
        let y = distance * Math.cos(bearing * Math.PI / 180);
        let x = distance * Math.sin(bearing * Math.PI / 180);
        return [x / 100, y / 100]
    }

    //方法2 通过d3-geo
    // caculateRelativePosition(start){
    //     if(!this.projection){
    //         this.projection = d3.geoMercator().center([117.649,39.019]);
    //     }
    //     const [x,y] = this.projection(start);
    //     return [x,y]

    // }

    render() {
        return (
            <div id="space">




            </div>
        )
    }

}
