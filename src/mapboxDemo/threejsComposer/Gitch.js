import React, { Component } from 'react';
import * as THREE from 'three';
import { MapControls } from 'three/examples/jsm/controls/OrbitControls';
import {RenderPass} from 'three/examples/jsm/postprocessing/RenderPass';
import {GlitchPass} from 'three/examples/jsm/postprocessing/GlitchPass';


import {EffectComposer} from 'three/examples/jsm/postprocessing/EffectComposer';
import {GUI} from 'three/examples/jsm/libs/dat.gui.module';

export default class Gitch extends Component {
    constructor() {
        super();
        this.animate = this.animate.bind(this);
        this.handleResize = this.handleResize.bind(this);
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.s = this.width / this.height;
        this.clock = new THREE.Clock();
    }
    componentDidMount() {
        this.init();
        window.addEventListener('resize',this.handleResize)
    }
    init() {
        // initScene
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0x222222);



        // initCamera
        this.camera = new THREE.PerspectiveCamera(45, this.s, 0.1, 1000);
        // this.camera.position.set(-10, 15, 25);
        this.camera.position.set(-10, 15, 25);

        this.camera.lookAt(new THREE.Vector3(0, 0, 0));
        this.camera.lookAt(0, 0, 0);
        this.camera.lookAt(this.scene.position);
        console.log(this.camera);

        this.scene.add(this.camera);
        const cameraHelper = new THREE.CameraHelper(this.camera);
        this.scene.add(cameraHelper);

        // initLight
        const ambi = new THREE.AmbientLight(0x181818);
        this.scene.add(ambi);
        const spotLight = new THREE.DirectionalLight(0xffffff);
        spotLight.position.set(550, 100, 500);
        spotLight.intensity = 0.6;
        this.scene.add(spotLight);






        // initRender
        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.width, this.height);
        this.renderer.setClearColor(0xb9d3ff, 1);
        document.getElementById('space').appendChild(this.renderer.domElement);


        //effectComposer
        const renderPass = new RenderPass(this.scene, this.camera);
        const effectGlitch = new GlitchPass(64);
        effectGlitch.renderToScreen = true;
        this.composer = new EffectComposer(this.renderer);
        this.composer.addPass(renderPass);
        this.composer.addPass(effectGlitch);


        //setup the control gui
        const controls = new function () {
            this.goWild = false;
            this.updateEffect = function(){
                effectGlitch.goWild = controls.goWild;
            };
        };
        const gui = new GUI();
        gui.add(controls, "goWild").onChange(controls.updateEffect);



        //add axesHelper
        let axesHelper = new THREE.AxesHelper(250);
        this.scene.add(axesHelper);

        //addControl
        this.addControl();



        //initSphere
        this.createMesh()

        //addControl
        this.addControl();



        this.animate();






    }
    createMesh() {
        const planetTexture = new THREE.TextureLoader().load('./texture/planets/Earth.png');
        const specularTexture = THREE.ImageUtils.loadTexture('./texture/planets/EarthSpec.png');
        const normalTexture = THREE.ImageUtils.loadTexture('./texture/planets/EarthNormal.png');

        const sphereGeometry = new THREE.SphereGeometry(10, 40, 40);
        const sphereMaterial = new THREE.MeshPhongMaterial({
            map: planetTexture,
            normalMap: normalTexture,
            specularMap: specularTexture,
            specular: new THREE.Color(0x4444aa),//高光的颜色
            shininess: 10//高光的亮度
        });
        const mesh = new THREE.Mesh(sphereGeometry, sphereMaterial);
        this.scene.add(mesh);
    }
    addControl() {
        const controls = new MapControls(this.camera, this.renderer.domElement);
        controls.enableDamping = true;
        controls.dampingFactor = 0.25;
        controls.screenSpacePanning = false;
        controls.maxDistance = 5000;
        controls.addEventListener('change', () => {
            this.renderer.render(this.scene, this.camera);
        });
    }

    animate() {
        let delta = this.clock.getDelta();
        // this.renderer.render(this.scene, this.camera);
        this.composer.render(delta);
        requestAnimationFrame(this.animate);
    }
    handleResize(){

        this.renderer.setSize(this.width, this.height);
        this.camera.aspect = this.s;
        this.camera.updateProjectionMatrix();
    }


    render() {
        return (
            <div id='space'>

            </div>
        )
    }
}
