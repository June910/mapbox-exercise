import React, { Component } from 'react';
import * as THREE from 'three';
import { MapControls } from 'three/examples/jsm/controls/OrbitControls';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { GlitchPass } from 'three/examples/jsm/postprocessing/GlitchPass';


import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { GUI } from 'three/examples/jsm/libs/dat.gui.module';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader';
import { SepiaShader } from 'three/examples/jsm/shaders/SepiaShader';
import { ColorifyShader } from 'three/examples/jsm/shaders/ColorifyShader';
import { MaskPass, ClearMaskPass } from 'three/examples/jsm/postprocessing/MaskPass';








export default class Mask extends Component {
    constructor() {
        super();
        this.animate = this.animate.bind(this);
        this.handleResize = this.handleResize.bind(this);
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.s = this.width / this.height;
        this.clock = new THREE.Clock();
    }
    componentDidMount() {
        this.init();
        window.addEventListener('resize', this.handleResize)
    }
    init() {
        // initScene
        this.sceneEarth = new THREE.Scene();
        this.sceneMars = new THREE.Scene();
        this.sceneBG = new THREE.Scene();
        // this.sceneBG.background = new THREE.TextureLoader().load('./texture/starry-deep-outer-space-galaxy.jpg');

        // this.scene.background = new THREE.Color(0x222222);



        // initCamera
        this.camera = new THREE.PerspectiveCamera(45, this.s, 0.1, 1000);
        this.camera.position.set(-10, 15, 25);
        this.camera.lookAt(new THREE.Vector3(0, 0, 0));
        this.cameraBG = new THREE.OrthographicCamera(-this.width, this.width, this.height, -this.height, -10000, 10000);
        this.cameraBG.position.z = 50;



        this.camera.lookAt(new THREE.Vector3(0, 0, 0));


        this.sceneBG.add(this.cameraBG);
        const cameraHelper = new THREE.CameraHelper(this.camera);
        this.sceneBG.add(cameraHelper);

        //initSphere
        this.earth = this.createEarthMesh(new THREE.SphereGeometry(10, 40, 40))
        this.earth.position.x = -10;
        this.sceneEarth.add(this.earth);
        this.mars = this.createMarsMesh(new THREE.SphereGeometry(5, 40, 40))
        this.mars.position.x = 10;
        this.sceneMars.add(this.mars);

        // initLight
        const ambi1 = new THREE.AmbientLight(0x181818);
        const ambi2 = new THREE.AmbientLight(0x181818);
        this.sceneEarth.add(ambi1);
        this.sceneMars.add(ambi2);
        const spotLight1 = new THREE.DirectionalLight(0xffffff);
        spotLight1.position.set(550, 100, 500);
        spotLight1.intensity = 0.6;
        this.sceneEarth.add(spotLight1);

        const spotLight2 = new THREE.DirectionalLight(0xffffff);
        spotLight2.position.set(550, 100, 500);
        spotLight2.intensity = 0.6;
        this.sceneMars.add(spotLight2);
        // this.sceneBG.add(ambi1);


        const bgPlaneGeometry = new THREE.PlaneGeometry(1, 1);
        const bgMaterial = new THREE.MeshBasicMaterial({
            map: new THREE.TextureLoader().load('./texture/starry-deep-outer-space-galaxy.jpg'),
            depthTest: false
        });
        const bgPlane = new THREE.Mesh(bgPlaneGeometry, bgMaterial);
        bgPlane.position.z = -1000;
        bgPlane.scale.set(this.width * 2, this.height * 2, 1);
        // this.sceneBG.add(bgPlane);






        // initRender
        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.width, this.height);
        // this.renderer.setClearColor(0xb9d3ff, 1);
        // new THREE.Color(0x000,1.0)
        document.getElementById('space').appendChild(this.renderer.domElement);

        const bgPass = new RenderPass(this.sceneBG, this.cameraBG);
        // bgPass.renderToScreen = true;
        const renderPass = new RenderPass(this.sceneEarth, this.camera);
        renderPass.clear = false;
        const renderPass2 = new RenderPass(this.sceneMars, this.camera);
        renderPass2.clear = false;

        const effectCopy = new ShaderPass(CopyShader);
        effectCopy.renderToScreen = true;

        const clearMask = new ClearMaskPass();
        const earthMask = new MaskPass(this.sceneEarth, this.camera);
        const marsMask = new MaskPass(this.sceneMars, this.camera);

        const effectSepia = new ShaderPass(SepiaShader);
        effectSepia.uniforms['amount'].value = 0.5;

        const effectColorify = new ShaderPass(ColorifyShader);
        effectColorify.uniforms['color'].value.setRGB(0.5, 0.5, 1);

        this.composer = new EffectComposer(this.renderer);
        this.composer.renderTarget1.stencilBuffer = true;
        this.composer.renderTarget2.stencilBuffer = true;
        this.composer.addPass(bgPass);
        this.composer.addPass(renderPass);
        this.composer.addPass(renderPass2);
        // this.composer.addPass(marsMask);
        this.composer.addPass(effectColorify);
        this.composer.addPass(clearMask);
        // this.composer.addPass(earthMask);
        // this.composer.addPass(effectSepia);
        // this.composer.addPass(clearMask);
        this.composer.addPass(effectCopy);








        //add axesHelper
        let axesHelper = new THREE.AxesHelper(250);
        // this.sceneBG.add(axesHelper);

        //addControl
        this.addControl();




        //addControl
        this.addControl();



        this.animate();

    }
    createEarthMesh(geom) {
        const planetTexture = new THREE.TextureLoader().load('./texture/planets/Earth.png');
        const specularTexture = THREE.ImageUtils.loadTexture('./texture/planets/EarthSpec.png');
        const normalTexture = THREE.ImageUtils.loadTexture('./texture/planets/EarthNormal.png');
        const sphereMaterial = new THREE.MeshPhongMaterial({
            map: planetTexture,
            normalMap: normalTexture,
            specularMap: specularTexture,
            specular: new THREE.Color(0x4444aa),//高光的颜色
            // shininess: 20//高光的亮度
        });
        const mesh = new THREE.Mesh(geom, sphereMaterial);
        return mesh;
        // this.scene.add(mesh);
    }
    createMarsMesh(geom) {
        const planetTexture = new THREE.TextureLoader().load('./texture/planets/Mars_2k-050104.png');
        const normalTexture = THREE.ImageUtils.loadTexture('./texture/planets/Mars-normalmap_2k.png');
        const sphereMaterial = new THREE.MeshPhongMaterial({
            map: planetTexture,
            normalMap: normalTexture,
            specular: new THREE.Color(0x4444aa),//高光的颜色
            shininess: 10//高光的亮度
        });
        const mesh = new THREE.Mesh(geom, sphereMaterial);
        return mesh;
    }
    addControl() {
        this.controls = new MapControls(this.camera, this.renderer.domElement);
        this.controls.autoRotate = false;
        this.controls.enableDamping = true;
        this.controls.dampingFactor = 0.25;
        this.controls.screenSpacePanning = false;
        this.controls.maxDistance = 5000;
        // controls.addEventListener('change', () => {
        //     this.renderer.render(this.sceneBG, this.cameraBG);
        // });
    }

    animate() {
        let delta = this.clock.getDelta();
        this.controls.update(delta);
        // this.renderer.render(this.sceneBG, this.cameraBG);
        this.earth.rotation.y += 0.002;
        this.mars.rotation.y += 0.002;
        requestAnimationFrame(this.animate);
        this.composer.render(delta);

    }
    handleResize() {

        this.renderer.setSize(this.width, this.height);
        this.camera.aspect = this.s;
        this.camera.updateProjectionMatrix();
    }


    render() {
        return (
            <div id='space'>

            </div>
        )
    }
}
