import React, { Component } from 'react';
import * as THREE from 'three';
import { MapControls } from 'three/examples/jsm/controls/OrbitControls';
import {RenderPass} from 'three/examples/jsm/postprocessing/RenderPass';
import {FilmPass} from 'three/examples/jsm/postprocessing/FilmPass';
import {BloomPass} from 'three/examples/jsm/postprocessing/BloomPass';
import {ShaderPass} from 'three/examples/jsm/postprocessing/ShaderPass';
import {CopyShader} from 'three/examples/jsm/shaders/CopyShader';
import {DotScreenPass} from 'three/examples/jsm/postprocessing/DotScreenPass';

import {EffectComposer} from 'three/examples/jsm/postprocessing/EffectComposer';
import {GUI} from 'three/examples/jsm/libs/dat.gui.module';

export default class BasicEffectComposer extends Component {
    constructor() {
        super();
        this.animate = this.animate.bind(this);
        this.handleResize = this.handleResize.bind(this);
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.s = this.width / this.height;
        this.clock = new THREE.Clock();
    }
    componentDidMount() {
        this.init();
        window.addEventListener('resize',this.handleResize)
    }
    init() {
        // initScene
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0x222222);



        // initCamera
        this.camera = new THREE.PerspectiveCamera(45, this.s, 0.1, 1000);
        // this.camera.position.set(-10, 15, 25);
        this.camera.position.set(-10, 15, 25);

        this.camera.lookAt(new THREE.Vector3(0, 0, 0));
        this.camera.lookAt(0, 0, 0);
        this.camera.lookAt(this.scene.position);

        this.scene.add(this.camera);
        const cameraHelper = new THREE.CameraHelper(this.camera);
        this.scene.add(cameraHelper);

        // initLight
        const ambi = new THREE.AmbientLight(0x181818);
        this.scene.add(ambi);
        const spotLight = new THREE.DirectionalLight(0xffffff);
        spotLight.position.set(550, 100, 500);
        spotLight.intensity = 0.6;
        this.scene.add(spotLight);


        // initRender
        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.width, this.height);
        this.renderer.setClearColor(0xb9d3ff, 1);

        document.getElementById('space').appendChild(this.renderer.domElement);


        //effectComposer
        const renderPass = new RenderPass(this.scene, this.camera);
        const effectCopy = new ShaderPass(CopyShader);
        effectCopy.renderToScreen = true;



        var dotScreenPass = new DotScreenPass();

        const effectFilm = new FilmPass(0.8, 0.325, 256, false);
        effectFilm.renderToScreen = true;



        this.composer = new EffectComposer(this.renderer);
        this.composer.addPass(renderPass);
        this.composer.addPass(effectCopy);



        let bloomPass = new BloomPass(3, 10, 0.5, 512);
        // bloomPass.renderToScreen = true;


        this.composer1 = new EffectComposer(this.renderer);
        this.composer1.addPass(renderPass);
        this.composer1.addPass(dotScreenPass);
        this.composer1.addPass(effectCopy);


        this.composer2 = new EffectComposer(this.renderer);
        this.composer2.addPass(renderPass);
        this.composer2.addPass(effectCopy);


        this.composer3 = new EffectComposer(this.renderer);
        this.composer3.addPass(renderPass);
        this.composer3.addPass(bloomPass);
        this.composer3.addPass(effectCopy);

        this.composer4 = new EffectComposer(this.renderer);
        this.composer4.addPass(renderPass);
        this.composer4.addPass(effectFilm);



        //setup the control gui
        const that = this;
        const controls = new function () {

            // film
            this.scanlinesCount = 256;
            this.grayscale = false;
            this.scanlinesIntensity = 0.3;
            this.noiseIntensity = 0.8;

            // bloompass
            this.strength = 3;
            this.kernelSize = 25;
            this.sigma = 5.0;
            this.resolution = 256;

            // dotscreen
            this.centerX = 0.5;
            this.centerY = 0.5;
            this.angle = 1.57;
            this.scale = 1;

            this.updateEffectFilm = function () {

                effectFilm.uniforms.grayscale.value = controls.grayscale;
                effectFilm.uniforms.nIntensity.value = controls.noiseIntensity;
                effectFilm.uniforms.sIntensity.value = controls.scanlinesIntensity;
                effectFilm.uniforms.sCount.value = controls.scanlinesCount;
            };

            this.updateDotScreen = function () {
                dotScreenPass = new DotScreenPass(new THREE.Vector2(controls.centerX, controls.centerY), controls.angle, controls.scale);
                that.composer1 = new EffectComposer(that.renderer);
                that.composer1.addPass(renderPass);
                that.composer1.addPass(dotScreenPass);
                that.composer1.addPass(effectCopy);
            };

            this.updateEffectBloom = function () {
                console.log(this);
                console.log(this.strength);
                bloomPass = new BloomPass(controls.strength, controls.kernelSize, controls.sigma, controls.resolution);
                that.composer3 = new EffectComposer(that.renderer);
                that.composer3.addPass(renderPass);
                that.composer3.addPass(bloomPass);
                that.composer3.addPass(effectCopy);
            };
        };


        const gui = new GUI();

        const bpFolder = gui.addFolder("BloomPass");
        bpFolder.add(controls, "strength", 1, 10).onChange(controls.updateEffectBloom);
        bpFolder.add(controls, "kernelSize", 1, 100).onChange(controls.updateEffectBloom);
        bpFolder.add(controls, "sigma", 1, 10).onChange(controls.updateEffectBloom);
        bpFolder.add(controls, "resolution", 0, 1024).onChange(controls.updateEffectBloom);


        var fpFolder = gui.addFolder("FilmPass");
        fpFolder.add(controls, "scanlinesIntensity", 0, 1).onChange(controls.updateEffectFilm);
        fpFolder.add(controls, "noiseIntensity", 0, 3).onChange(controls.updateEffectFilm);
        fpFolder.add(controls, "grayscale").onChange(controls.updateEffectFilm);
        fpFolder.add(controls, "scanlinesCount", 0, 2048).step(1).onChange(controls.updateEffectFilm);

        var dsFolder = gui.addFolder("DotScreenPass");
        dsFolder.add(controls, "centerX", 0, 1).onChange(controls.updateDotScreen);
        dsFolder.add(controls, "centerY", 0, 1).onChange(controls.updateDotScreen);
        dsFolder.add(controls, "angle", 0, 3.14).onChange(controls.updateDotScreen);
        dsFolder.add(controls, "scale", 0, 10).onChange(controls.updateDotScreen);



        //add axesHelper
        let axesHelper = new THREE.AxesHelper(250);
        this.scene.add(axesHelper);

        //addControl
        this.addControl();



        //initSphere
        this.createMesh()

        //addControl
        this.addControl();



        this.animate();






    }
    createMesh() {
        const planetTexture = new THREE.TextureLoader().load('./texture/planets/Earth.png');
        const specularTexture = THREE.ImageUtils.loadTexture('./texture/planets/EarthSpec.png');
        const normalTexture = THREE.ImageUtils.loadTexture('./texture/planets/EarthNormal.png');
        console.log(planetTexture);

        const sphereGeometry = new THREE.SphereGeometry(10, 40, 40);
        const sphereMaterial = new THREE.MeshPhongMaterial({
            map: planetTexture,
            normalMap: normalTexture,
            specularMap: specularTexture,
            specular: new THREE.Color(0x4444aa),//高光的颜色
            shininess: 10//高光的亮度
        });
        const mesh = new THREE.Mesh(sphereGeometry, sphereMaterial);
        this.scene.add(mesh);
    }
    addControl() {
        const controls = new MapControls(this.camera, this.renderer.domElement);
        controls.enableDamping = true;
        controls.dampingFactor = 0.25;
        controls.screenSpacePanning = false;
        controls.maxDistance = 5000;
        controls.addEventListener('change', () => {
            console.log(this.camera.zoom);
            this.renderer.render(this.scene, this.camera);
        });
    }

    animate() {
        let delta = this.clock.getDelta();
        this.renderer.autoClear = false;
        this.renderer.clear();
        // this.renderer.render(this.scene, this.camera);
        const halfWidth = this.width / 2;
        const halfHeight = this.height / 2;

        this.renderer.setViewport(0, 0, 2 * halfWidth, 2 * halfHeight);
        this.composer.render(delta);

            this.renderer.setViewport(0, 0, halfWidth, halfHeight);
            this.composer1.render(delta);

            this.renderer.setViewport(halfWidth, 0, halfWidth, halfHeight);
            this.composer2.render(delta);

            this.renderer.setViewport(0, halfHeight, halfWidth, halfHeight);
            this.composer3.render(delta);

            this.renderer.setViewport(halfWidth, halfHeight, halfWidth, halfHeight);
            this. composer4.render(delta);



        requestAnimationFrame(this.animate);
    }
    handleResize(){

        this.renderer.setSize(this.width, this.height);
        this.camera.aspect = this.s;
        this.camera.updateProjectionMatrix();
    }


    render() {
        return (
            <div id='space'>

            </div>
        )
    }
}
