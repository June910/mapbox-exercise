import React, { Component } from 'react';
import * as THREE from 'three';
import { MapControls } from 'three/examples/jsm/controls/OrbitControls';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { GlitchPass } from 'three/examples/jsm/postprocessing/GlitchPass';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { OutlinePass } from 'three/examples/jsm/postprocessing/OutlinePass';

import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass';
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { GUI } from 'three/examples/jsm/libs/dat.gui.module';
import shader from './shaders/selectObject';

export default class SelectObject extends Component {
    constructor() {
        super();
        this.animate = this.animate.bind(this);
        this.handleResize = this.handleResize.bind(this);
        this.handleClick = this.handleClick.bind(this);

        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.s = this.width / this.height;
        this.clock = new THREE.Clock();
        this.mouse = {};
        this.rayCaster = new THREE.Raycaster();
        this.selectedObject = null;
    }
    componentDidMount() {
        this.init();
        window.addEventListener('resize', this.handleResize);
        window.addEventListener('click',this.handleClick);
    }
    init() {
        // initScene
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0x222222);



        // initCamera
        this.camera = new THREE.PerspectiveCamera(45, this.s, 0.1, 1000);
        // this.camera.position.set(-10, 15, 25);
        this.camera.position.set(-10, 15, 25);

        this.camera.lookAt(new THREE.Vector3(0, 0, 0));
        this.camera.lookAt(0, 0, 0);
        this.camera.lookAt(this.scene.position);
        console.log(this.camera);

        this.scene.add(this.camera);
        const cameraHelper = new THREE.CameraHelper(this.camera);
        // this.scene.add(cameraHelper);

        // initLight
        const ambi = new THREE.AmbientLight(0x181818);
        this.scene.add(ambi);
        const spotLight = new THREE.DirectionalLight(0xffffff);
        spotLight.position.set(550, 100, 500);
        spotLight.intensity = 0.6;
        this.scene.add(spotLight);

        // initRender
        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.width, this.height);
        this.renderer.setClearColor(0xb9d3ff, 1);
        document.getElementById('space').appendChild(this.renderer.domElement);


        //effectComposer
        var params = {
            exposure: 1,
            bloomStrength: 5,
            bloomThreshold: 0,
            bloomRadius: 0,
            scene: "Scene with Glow"
        };
        const renderPass = new RenderPass(this.scene, this.camera);
        const bloomPass = new UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHiehgt), 1.5, 0.4, 0.85);
        bloomPass.threshold = params.bloomThreshold;
        bloomPass.strength = params.bloomStrength;
        bloomPass.radius = params.bloomRadius;

        const copyShaderPass = new ShaderPass(CopyShader);
        // bloomPass.renderToScreen = true;

        this.bloomComposer = new EffectComposer(this.renderer);
        this.bloomComposer.addPass(renderPass);
        // this.bloomComposer.addPass(bloomPass);
        // this.composer.addPass(copyShaderPass);


        const finalPass = new ShaderPass(shader);
        finalPass.uniforms.bloomTexture.value = this.bloomComposer.renderTarget2.texture;
        this.finalComposer = new EffectComposer(this.renderer);
        this.finalComposer.addPass(renderPass);
        this.finalComposer.addPass(finalPass);

        this.outlinePass = new OutlinePass(new THREE.Vector2(this.width, this.height), this.scene, this.camera);
        this.outlinePass.renderToScreen = true;

        this.outlinePass.visibleEdgeColor = new THREE.Color(1,0,0);
        this.outlinePass.hiddenEdgeColor = new THREE.Color(0, 1, 0);
        this.outlinePass.edgeGlow = 0.0;
        this.outlinePass.usePatternTexture = false;
        this.outlinePass.edgeThickness = 1.0;
        this.outlinePass.edgeStrength = 6.0;
        this.outlinePass.downSampleRatio = 2;
        this.outlinePass.pulsePeriod = 0;

        this.outlineComposer = new EffectComposer(this.renderer);
        this.outlineComposer.addPass(renderPass);
        this.outlineComposer.addPass(this.outlinePass);







        //setup the control gui
        // const controls = new function () {
        //     this.goWild = false;
        //     this.updateEffect = function(){
        //         effectGlitch.goWild = controls.goWild;
        //     };
        // };
        // const gui = new GUI();
        // gui.add(controls, "goWild").onChange(controls.updateEffect);



        //add axesHelper
        let axesHelper = new THREE.AxesHelper(250);
        // this.scene.add(axesHelper);

        //addControl
        this.addControl();



        //initSphere
        this.createMesh()

        //addControl
        this.addControl();



        this.animate();






    }
    createMesh() {
        const geometry = new THREE.IcosahedronBufferGeometry(1, 4);
        for (let i = 0; i < 50; i++) {
            let color = new THREE.Color();
            color.setHSL(Math.random(), 0.7, Math.random() * 0.2 + 0.05);
            const material = new THREE.MeshBasicMaterial({
                color
            });
            const sphere = new THREE.Mesh(geometry, material);
            sphere.position.x = Math.random() * 10 - 5;
            sphere.position.y = Math.random() * 10 - 5;
            sphere.position.z = Math.random() * 10 - 5;
            sphere.position.normalize().multiplyScalar(Math.random() * 0.4 + 2.0);
            sphere.scale.setScalar(Math.random() * Math.random() + 0.5);
            this.scene.add(sphere);


        }
        console.log(this.scene.children);

    }
    addControl() {
        const controls = new MapControls(this.camera, this.renderer.domElement);
        controls.enableDamping = true;
        controls.dampingFactor = 0.25;
        controls.screenSpacePanning = false;
        controls.maxDistance = 5000;
        controls.addEventListener('change', () => {
            this.renderer.render(this.scene, this.camera);
        });
    }

    animate() {
        let delta = this.clock.getDelta();
        // this.renderer.render(this.scene, this.camera);
        // this.bloomComposer.render(delta);
        // this.finalComposer.render(delta);
        this.outlineComposer.render(delta);
        requestAnimationFrame(this.animate);
    }
    handleResize() {

        this.renderer.setSize(this.width, this.height);
        this.camera.aspect = this.s;
        this.camera.updateProjectionMatrix();
    }
    handleClick(e){
        e.preventDefault();
        this.mouse.x = (e.clientX / this.width) * 2 - 1;
        this.mouse.y = -(e.clientY / this.height) * 2 + 1;
        this.fire(this.mouse);

    }
    fire(mouse){
        this.rayCaster.setFromCamera(mouse,this.camera);
        let intersects = this.rayCaster.intersectObjects(this.scene.children);
        let mesh = intersects[0]?intersects[0].object:null;
        if(mesh && mesh.isMesh){
            if(this.selectedObject === mesh){
                this.outlinePass.selectedObjects = [];
                this.selectedObject = null;
            }else{
                this.selectedObject = mesh;
                this.outlinePass.selectedObjects = [mesh];

            }
            // mesh.isMesh?this.outlinePass.selectedObjects = [mesh] : return true
        }
    }


    render() {
        return (
            <div id='space'>

            </div>
        )
    }
}
