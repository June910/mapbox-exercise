import React from 'react'
import {useState, useReducer} from 'react';

export default function ReducerPage() {
    // const [state, dispatch] = useReducer(reducer, initialState, init);
    const [fruits, setFruits] = useState(["apple","banana","pear"]);
    

    return (
        <div>
            <FruitAdd fruits={fruits} setFruits = {setFruits}/>
            <FruitList fruits={fruits} setFruits = {setFruits}/>
        </div>
    )
}
function FruitList(props){
    const {fruits, setFruits} = props;//这里不把useState写在里面的原因是Fruit还会有其他组件操作fruits，多个组件保持状态共享
    const delFruit = (index)=>{
        let tmp = [...fruits];
        tmp.splice(index,1);
        setFruits(tmp);
    }
    return (
        <div>
            <ul>
                {fruits.map((fruit,index)=>{
                    return <li onClick={()=>{delFruit(index)}} key={index}>{fruit}</li>
                })}
            </ul>
        </div>
    )
}
function FruitAdd(props){
    const {fruits, setFruits} = props;//这里不把useState写在里面的原因是Fruit还会有其他组件操作fruits，多个组件保持状态共享
    const [name, setName] = useState("");
    const addFruit = ()=>{
        setFruits([...fruits,name])
    }
    return (
        <div>
            <input type="text" onChange = {(e)=>{setName(e.target.value)}} />
            <button onClick={addFruit}>增加</button>
        </div>
    )
}
