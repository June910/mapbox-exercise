import React, { Component } from 'react';
import {connect} from 'react-redux';
import {login} from '../store/action/userAction';
import {Redirect} from 'react-router-dom';
class Login extends Component {
    render() {
        const {isLogin,location,logout,login} = this.props;
        const {redirect='/'} = location.state || {};

        if(isLogin){
            return <Redirect to={redirect}></Redirect>
        }
        return (
            <div>
                <h2>Login</h2>
                <button onClick={login}>
                    登录
                </button>
               
            </div>
        )
    }
}
export default connect(
    (state)=>state.user,
    {login}
)(Login)
