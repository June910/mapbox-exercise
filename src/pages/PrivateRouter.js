import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from "react-router-dom";


class PrivateRouter extends Component {
    render() {
        console.log('privateRouterPage',this.props);
        const {path,component,isLogin} = this.props;
        if(isLogin){
            return <Route to={path} component={component}/>
        }
        else{
            return <Redirect to={{
                pathname:'/login',
                state:{
                    redirect:path
                }

            }
            }></Redirect>
        }
        return (
            <div>
                
            </div>
        )
    }
}
export default connect(
    state=>state.user
)(PrivateRouter)