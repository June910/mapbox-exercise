import React, { Component } from 'react';
import {BrowserRouter, Link, Route,Switch} from "react-router-dom";
import {connect} from 'react-redux';
import {logout} from '../store/action/userAction';
import { Redirect } from 'react-router-dom/cjs/react-router-dom.min';


class User extends Component {
    render() {
        const {logout,isLogin} = this.props;
        console.log('userPage',this.props);
        if(!isLogin){
            return <Redirect to={{
                pathname:'/login',
                state:{
                    redirect:'/user'
                }

            }
            }></Redirect>
        }
        return (
            <div>
                <h2>User</h2>
                <Link to="/user/user1">user1</Link>
                <Link to="/user/user2">user2</Link>
                <Route path="/user/user1/:id" component = {User1}></Route>
                <Route path="/user/user2" component = {()=><div>user2</div>}></Route>
                <button onClick = {logout}>退出</button>
            </div>
        )
    }
}

function User1(props){
const {id} = props.match.params;
console.log(id);
    return (
        <div>
            <span>{`这里是User${id}`}</span>
        </div>
    )
}

export default connect(
    state=>state.user,
    {logout}
)(User)
