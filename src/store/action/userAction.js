const login = ()=>{
    return {type:'loginSuccess'}
}
const logout = ()=>{
    return {type:'getUserInfo'}
}
export {login,logout}