import { createStore, combineReducers, applyMiddleware } from "redux";
import counterReducer from './reducer/counterReducer';
import styleReducer from "./reducer/styleReducer";
import userReducer from './reducer/userReducer';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
const store = createStore(combineReducers({
    style: styleReducer,
    counter: counterReducer,
    user: userReducer 
}),
    applyMiddleware(logger, thunk)
);
export default store;