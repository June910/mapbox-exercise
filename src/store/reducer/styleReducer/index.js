import styleOptions from './styleOptions';
const initStyle = {
    styleOptions,
    active:styleOptions[0]
}
const styleReducer = (state=initStyle,action)=>
{
    switch(action.type){
        case 'SET_ACTIVE_STYLE':
            console.log('执行');
            return Object.assign({},state,{active:action.option});
        default:
            return state;
    }
}


export default styleReducer;